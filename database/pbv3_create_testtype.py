#!/usr/bin/env python

import os, sys
import argparse
import yaml
import itkdb
import pprint
from database import pwbdbtools

def create_testtype(client, code, name, description, schemapath):
    """
    Create a new test type following the Powerboard test type template.

    The template has the following properties:
    - CONFIG: points to a CONFIG test type with the AMAC configuration

    The template has the following parameters:
    - TIMESTART: Start time of the test in format `%Y_%m_%d-%H:%M:%SZ%z`
    - TIMEEND: End time of the test in format `%Y_%m_%d-%H:%M:%SZ%z`
    """

    # Query template
    query={
        'project'         : 'S',
        'componentType'   : 'PWB',
        'code'            : code,
        'name'            : name,
        'description'     : description,
        'changeStage'     : False,
        'automaticGrading': False,
        'properties'      : [
            {
                'code'       :'CONFIG',
                'name'       :'AMACv2 Configuration',
                'description':'Configuration Test Run Reference',
                'dataType'   :'string',
                'valueType'  :'single',
                'required'   :True
            }
        ],
        'parameters'      : [
            {
                'code'       :'TIMESTART',
                'name'       :'TIMESTART',
                'description':'Time when the test started',
                'dataType'   :'string',
                'valueType'  :'single',
                'required'   :True
            },
            {
                'code'       :'TIMEEND',
                'name'       :'TIMEEND',
                'description':'Time when the test ended',
                'dataType'   :'string',
                'valueType'  :'single',
                'required'   :True
            },
        ]
    }

    # Parse schema
    schema=yaml.load(open(schemapath), Loader=yaml.FullLoader)

    for param in schema.get('parameters',[]):
        paramschema=schema['parameters'][param]
        paramschema['code']=param
        query['parameters'].append(paramschema)

    for param in schema.get('properties',[]):
        paramschema=schema['properties'][param]
        paramschema['code']=param
        query['properties'].append(paramschema)

    # Upload
    print(f'Create new test type: {code}')
    c.post('importTestType', json=query)
        
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Create a new test type following a Powerboard templeate")
    parser.add_argument('code'       , help="New test type code.")
    parser.add_argument('name'       , help="New test type name.")
    parser.add_argument('description', help="New test type description.")
    parser.add_argument('schemapath' , help="Path to schema file.")

    args = parser.parse_args()

    c = pwbdbtools.get_db_client()

    create_testtype(c, args.code, args.name, args.description, args.schemapath)
