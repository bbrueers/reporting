#!/usr/bin/env python
# coding: utf-8

# In[15]:


import os
from pathlib import Path
import sys
import argparse
import json
from glob import glob
import mimetypes as mt

import itkdb



def disassembly(client, serialnumber):
    parent = client.get('getComponent', json={"component": serialnumber})
    print("Parent component: "+parent['code'])
    children = parent["children"]
    #print(json.dumps(children, indent=3))
    #if children[0]["identity"] == None:
    #    print("No child assembled") 
    
    #for child in children:
    #    print(child["order"], child["identity"])
    #    if child["component"] == None: print ("Child is gone")
    
    
    for child in children:
        #print(child["component"]["code"])
        if child["component"] != None:
             
            print("Disassemble: ", child["component"]["serialNumber"])

            disassemble_container = client.post('disassembleComponent', json={
                "parent": parent["code"],
                "child": child["component"]["code"],
                "trashed": False
           })
    
    
    


# In[ ]:


if __name__ == "__main__":
    
    
    # Parse command line
    parser = argparse.ArgumentParser(description="Script for uploading strip hyrbid test results")
    parser.add_argument("serialNumber", help="ATLAS serial number of parent")
    parser.add_argument("--accessCode1", default = None, help = "Access Code 1 (can also be specified via ITKDB_ACCESS_CODE1 env var)")
    parser.add_argument("--accessCode2", default = None, help = "Access Code 2 (can also be specified via ITKDB_ACCESS_CODE2 env var)")
    args = parser.parse_args()


    # Authenticate
    if args.accessCode1 and args.accessCode2:
        u = itkdb.core.User(accessCode1=args.accessCode1, accessCode2=args.accessCode2)
        client = itkdb.Client(user = u)
    elif "ITKDB_ACCESS_CODE1" in os.environ and "ITKDB_ACCESS_CODE2" in os.environ:
        client = itkdb.Client()
    else:
        sys.exit("ERROR: Must provide access codes, either via arguments or environment variables")

    client.user.authenticate()
    user = client.get('getUser', json={'userIdentity': client.user.identity})
    print ('>>> Hello {} {}, welcome to the strip hybrid upload script'.format(user["firstName"], user["lastName"]))
    
    
    
    
    
    disassembly(client, args.serialNumber)

