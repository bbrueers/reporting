#!/usr/bin/env python

import os, sys
import argparse
import itkdb
import random

from database import pwbdb
from database import pwbdbtools
from database import transitions

import time

nExceptions=1

def make_pwb_parent(flex):
    """
    Create a Powerboard component for `flex` and assemble it with the
    corresponing flex. The version and batch are set to the same value
    as the parent flex array. The number is set to a unique number in
    the [9000,10000) range.

    Parameters:
      flex (Component): Flex component who's parent to create

    Return:
     component: New Powerboard component
    """

    # Check for existing parent
    try:
        return next(filter(lambda p: p.componentType=='PWB', flex.parents))
    except StopIteration:
        pass
    print('Creating Powerboard...')
    # Get existing PB_NUMBWE
    pfilt=[
        {
            'code': 'PB_NUMBWE',
            'operator': '>=',
            'value': 9000
        }
        ]
    r=flex.client.get('listComponentsByProperty', json={'project':'S', 'componentType':'PWB', 'propertyFilter':pfilt})
    existing=set([pwbdbtools.get_property(pb['properties'], 'PB_NUMBWE') for pb in r])

    # Get unique number
    bogus=None
    while not bogus:
        bogus=random.randint(9000,10000)
        if bogus in existing:
            bugus=None

    # Get parent version/batch
    flexarray=next(filter(lambda p: p.componentType=='PB_FLEX_ARRAY', flex.parents))
    version=flexarray.property('VERSION')
    batch  =flexarray.property('BATCH')

    # Create parent!
    newpwb={
        'project':'S',
        'subproject':'SB',
        'institution':'LBNL_STRIP_POWERBOARDS',
        'componentType':'PWB',
        'type':'B3',
        'serialNumber': pwbdbtools.create_serial(version, batch, bogus),
        'properties':{
            'VERSION': version,
            'BATCH': batch,
            'PB_NUMBWE': bogus
            }
        }
    r=flex.client.post('registerComponent',json=newpwb)
    powerboard=pwbdb.Component(r['component']['code'], client=flex.client)
    print('\t{}'.format(pwbdbtools.create_serial(version,batch,bogus)))

    # Assemble to make it official
    print('Assemble flex')
    powerboard.assemble(flex)
    print('\tOK')
    print('Flex has been assembled to powerboard: '+powerboard.code)
    return powerboard

def smdload_pb(powerboard, bpol_type='BPOL12V4', linpol_type='LINPOL12V', currentLocation = 'LBNL_STRIP_POWERBOARDS'):
    """
    SMD load a powerboard.
    
    Attach random linPOL and bPOL to Powerboard.

    Parameters:
     powerboard (Component): a powerboard
     bpol_type (str): bPOL subtype to add
     linpol_type (str): linPOL subtype to add
     currentLocation (str): filter for currentLocation of random components
    """
    global nExceptions

    print("Loading linPOL and bPOL to powerboard: "+powerboard.code)
    #
    # Assemble chips

    print("Found those children in the powerboad:")
    print(powerboard.children)

    # linPOL
    random10 = [0,1,2,3,4,5,6,7,8,9]
    if 'PWB_LINPOL' not in powerboard.children:
        print('Assemble linPOL')
        linPOLs10 = pwbdbtools.get_random_components(powerboard.client, 'SG', 'PWB_LINPOL', linpol_type, currentLocation, n=10)
        linPOL=pwbdb.Component(linPOLs10[random.choice(random10)], client=powerboard.client)
        if linPOL==None:
            print('\tUnable to find free linPOLs of type {}...'.format(linpol_type))
        else:
            while linPOL is None or linPOL.parents is not None:
                print('\tThe linPOL we found already has a parent, searching for another one... ('+linPOL.code+")")
                linPOLs10 = pwbdbtools.get_random_components(powerboard.client, 'SG', 'PWB_LINPOL', linpol_type, currentLocation, n=10)
                linPOL=pwbdb.Component(linPOLs10[random.choice(random10)], client=powerboard.client)
            try:
                powerboard.assemble(linPOL)
                print('\tOK')
            except Exception:
                print('\t Get an exception! Will try again later...')
                nExceptions += 1

    # bPOL
    if 'BPOL12V' not in powerboard.children:
        print('Assemble bPOL')
        bPOLs10=pwbdbtools.get_random_components(powerboard.client, 'SG', 'BPOL12V', bpol_type, currentLocation, n=10)
        bPOL=pwbdb.Component(bPOLs10[random.choice(random10)], client=powerboard.client)
        if bPOL==None:
            print('\tUnable to find free bPOLs of type {}...'.format(bpol_type))
        else:
            while bPOL is None or bPOL.parents is not None:
                print('\tThe bPOL we found already has a parent, searching for another one... ('+bPOL.code+")")
                bPOLs10=pwbdbtools.get_random_components(powerboard.client, 'SG', 'BPOL12V', bpol_type, currentLocation, n=10)
                bPOL=pwbdb.Component(bPOLs10[random.choice(random10)], client=powerboard.client)
            try:
                powerboard.assemble(bPOL)
                print('\tOK')
            except Exception:
                print('\t Get an exception! Will try again later...')
                nExceptions += 1

def smdload(flex, bpol_type='BPOL12V4', linpol_type='LINPOL12V', currentLocation = 'LBNL_STRIP_POWERBOARDS'):
    """
    SMD load a flex.
    
    1. Create parent Powerboard component with bogus PB_NUMBWE
    2. Attach flex to Powerboard.
    3. Attach random linPOL and bPOL to Powerboard.

    Parameters:
     flex (Component): Flex which should be assembled into a Powerboard
     bpol_type (str): bPOL subtype to add
     linpol_type (str): linPOL subtype to add
     currentLocation (str): filter for currentLocation of random components
    """
    global nExceptions
    # Check if flex has a parent, otherwise make a new component
    try:
        powerboard=next(filter(lambda p: p.componentType=='PWB', flex.parents))
    except StopIteration:
        powerboard=make_pwb_parent(flex)

    print("Loading linPOL and bPOL to powerboard: "+powerboard.code)
    #
    # Assemble chips

    print("Found those children in the powerboad:")
    print(powerboard.children)
    smdload_pb(powerboard, bpol_type, linpol_type, currentLocation)


def loop_smdload(panelid, client, bpol_type='BPOL12V4', linpol_type='LINPOL12V', currentLocation = 'LBNL_STRIP_POWERBOARDS'):
    global nExceptions
    nExceptions = 1
    panel = transitions.Panel(panelid, client)  
    # Loop over children
    while nExceptions > 0:
        nExceptions = 0
        for child in panel.children['PB_FLEX'].values():
            smdload(child, bpol_type=bpol_type, linpol_type=linpol_type, currentLocation=currentLocation)

        if nExceptions > 0:
            print("Got "+str(nExceptions)+" exceptions in this loop. Do it again now...")
        else:
            print("SMD load of all powerboards have been finished!")
        time.sleep(3)
 
def loop_smdload_pb(panelid, client, bpol_type='BPOL12V4', linpol_type='LINPOL12V', currentLocation = 'LBNL_STRIP_POWERBOARDS'):
    global nExceptions
    nExceptions = 1
    panel = transitions.Panel(panelid, client)  
    print("SMD load flex array "+panel.code)
    # Loop over children
    while nExceptions > 0:
        nExceptions = 0
        flex_all = []
        pb_all = []
        for i,child in panel.children['PB_FLEX'].items():
            flex_all.append(child)
        for i,child in panel.children['PWB'].items():
            pb_all.append(child)
        print("Found "+str(len(flex_all)) + " flexes and "+str(len(pb_all))+" powerboards in the flex array")
        idx_flex = 0
        for i in range(len(pb_all)):
            if len(pb_all[i].children['PB_FLEX']) == 0 and idx_flex < len(flex_all):
                print("Disassemble flex "+flex_all[idx_flex].code+" from flex array")
                panel.disassemble(flex_all[idx_flex])
                idx_flex = idx_flex + 1
                print("Assemble flex "+flex_all[i].code+" to powerboard "+pb_all[i].code)
                pb_all[i].assemble(flex_all[i])
            smdload_pb(pb_all[i], bpol_type=bpol_type, linpol_type=linpol_type, currentLocation=currentLocation)

        if nExceptions > 0:
            print("Got "+str(nExceptions)+" exceptions in this loop. Do it again now...")
        else:
            print("SMD load of all powerboards have been finished!")
        time.sleep(3)
   
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="SMD load a Powerboard.")

    parser.add_argument("panelid" , help="Panel identifier (VBBXXXX).")
    parser.add_argument("-b","--bpol"     , default='BPOL12V4', help="Assemble bPOL12V of given type.")
    parser.add_argument("-l","--linpol"   , default='LINPOL12V', help="Assemble linPOL12V of given type.")    
    parser.add_argument("-c","--currentLocation", default = 'LBNL_STRIP_POWERBOARDS', help="Filter random POLs by current location.")

    args = parser.parse_args()

    c = pwbdbtools.get_db_client()

    #
    # Find panel component
    loop_smdload(panelid=args.panelid, client=c, bpol_type=args.bpol, linpol_type=args.linpol, currentLocation=args.currentLocation)

