#!/usr/bin/env python

import os, sys
import argparse
import itkdb

from database import pwbdbtools
from database import transitions

def panel_transition(c, panelid, stage, force=False, pretend=False):

    if stage not in transitions.available:
        print('Transition to {} not available'.format(stage))
        print('Available:')
        for stage in transition.available:
            print('\t{}'.format(stage))
        return False
    transition=transitions.available[stage]


    #
    # Find panel component and get current stage
    panel = transitions.Panel(panelid, c)

    print('\t===>Transition panel {} of type {}'.format(panelid, panel.componentType))
    print('\tCurrent Stage: {}'.format(panel.currentStage))

    #
    # Check whether transition is possible
    if not force:
        print('Checking if transition is allowed...')
        if not transition.check_panel(panel):
            return False
        print('\tOK')

    #
    # Perform the transition
    if not pretend:
        transition.perform(panel)
    print('\t===>Transition finished for panel '+panelid+'<===')
    return True

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Transition to the next stage for a Panel.")

    parser.add_argument("panelid"      , help="Panel identifier (VBBXXXX).")
    parser.add_argument("stage"        , help="Code for next stage.")
    parser.add_argument("-f","--force" , action='store_true',
                        help="Force transition without consistency check.")
    parser.add_argument("-p","--pretend", action='store_true',
                        help="Perform consistency check only. Do not transition.")

    args = parser.parse_args()
    c = pwbdbtools.get_db_client()

    panel_transition(c, args.panelid, args.stage, args.force, args.pretend)

