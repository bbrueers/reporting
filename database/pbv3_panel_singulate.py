#!/usr/bin/env python

import os, sys
import argparse
import itkdb

from database import pwbdbtools
from database import pwbdb
from database import transitions

def singulate(client, panelid, PWB_CARRIER_BATCH):
    """
    Singulate a Flex Array in the database by performing
    the following steps:

    1. Obtain a carrier card from `PWB_CARRIER_BATCH` batch.
      - Throw error if none available
    2. Deassemble Flex components from Flex Array `panel`.
    3. Assemble Flex components on Carrier Card
    4. Set panel id on Carrier Card
    """
    
    panel = transitions.Panel(panelid, client=client)
    
    mycarrier = None

    if panel.componentType!='PB_FLEX_ARRAY':
        print("Panel {} () is already singulated!".format(panelid, panel.code))
        flex = transitions.Panel(panelid, client=client, flexOnly=True)
        if len(flex.children['PWB']) == 0 and len(flex.children['PB_TC']) == 0:
            # No PBs on flex array, do nothing
            return
        else:
            # PBs are still on flex array
            panel = transitions.Panel(panelid, client=client, flexOnly=True)
            carrier=client.get('getComponent', json={'component':panelid, 'alternativeIdentifier':True})
            mycarrier=pwbdb.Component(carrier['code'], client=panel.client)

    else:
        # Obtain current panel
        r=panel.client.get('getBatchByNumber',json={'project':'S','batchType':'PWB_CARRIER_BATCH','number':PWB_CARRIER_BATCH})
        readycarriers=filter(lambda c: c['trashed']==False and c['currentStage']['code']=='MANUFACTURED', r['components'])
        readycarrier_codes=map(lambda component: component['code'], readycarriers)

        mycarrier=pwbdb.Component(next(readycarrier_codes), client=panel.client)
        print('Found carrier card {}'.format(mycarrier.code))
        mycarrier.property('ID',panelid)
        panel.client.post('updateComponentSN',json={'component':mycarrier.code, 'serialNumber':"20USBPC"+str(panelid)})
    
    # Singulate
    print('Singulating array {}'.format(panelid))
    
    if len(panel.children['PWB']) == 0:
        # Deassemble Flex components from Flex Array
        for i,flex in panel.children['PB_FLEX'].items():
            print('{}: Singulate PB_FLEX {}'.format(i,flex.code))
            mycarrier.assemble(next(filter(lambda p: p.componentType=='PWB', flex.parents)), slot=i)
            panel.disassemble(flex)
    else:
        for i,powerboard in panel.children['PWB'].items():
            print('{}: Singulate PWB {}'.format(i,powerboard.code))
            panel.disassemble(powerboard)
            mycarrier.assemble(powerboard, slot=i)
    for i,tc in panel.children['PB_TC'].items():
        print('{}: Singulate PB_TC {}'.format(i,tc.code))
        panel.disassemble(tc)
        if i==0:
            mycarrier.assemble(tc, slot=i)

    # Change stage of carrier card to make it unavailable for next singulation
    mycarrier.currentStage='MAN_LOAD'


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Singulate a Flex Array into a Carrier Card.")

    parser.add_argument("panelid"          , help="Panel identifier (VBBXXXX).")
    parser.add_argument("PWB_CARRIER_BATCH", help="Code for next stage.")

    args = parser.parse_args()

    c = pwbdbtools.get_db_client()

    singulate(c, args.panelid, args.PWB_CARRIER_BATCH)
