#!/usr/bin/env python

import os, sys, time
import argparse
import itkdb
import random

from database import pwbdb
from database import pwbdbtools
from database import pwbdbpandas
from database import transitions

def loadwafer(client, panelid, position, wafer, waferpos, atlasSN=""):
    """
    Associate an AMAC to the Powerboard.
    
    Parameters:
     client (itkdb.Client): Authenticated ITk DB client
     panelid (str): Panel ID to load (VBBXXXX)
     position (int): Powerboard position
     wafer (str): Wafer ID
     waferpos (int): Chip position on wafer
     atlasSN (str): if not empty, use the AMAC matches with this atlas serial number
    """

    #
    # Find panel component
    panel = transitions.Panel(panelid, client=client) 

    pwb=panel.children['PWB'][position]
    if len(pwb.children['AMAC'])>0:
        print('Powerboard ({}) already has an AMACv2!'.format(pwb.code))
        return

    # Find the necessary AMAC
    chip_code=""
    if atlasSN == "":
        chips=pwbdbpandas.listComponentsByProperty(client, 'S',
                                                   componentType='AMAC',
                                                   propertyFilter=[
                                                       {'code':'WAFER_NAME','operator':'=','value':wafer   },
                                                       {'code':'AMACNUMBER' ,'operator':'=','value':waferpos},
                                                   ])

        ## Check whether something was found
        if len(chips.index)!=1:
            raise Exception('Returned non-1 AMACv2\'s with the corresponding wafer identification.')

        ## Check property filter
        chip=chips[(chips.WAFER_NAME==wafer)&(chips.AMACNUMBER==waferpos)]
        if len(chip.index)==0:
            raise Exception('Unable to find an AMACv2 with the corresponding wafer identification.')

        chip_code = chip.iloc[0].code
    else:
        chip_comp = client.get('getComponent', json={'component':atlasSN})
        chip_code = chip_comp['code']
    chip=pwbdb.Component(chip_code, client=client)
    parents = chip.parents
    if parents is not None and len(parents)>0:
        for parent in parents:
            if parent.dump['componentType']['code'] == 'PWB':
                raise Exception('Found AMACv2 ({}) already has a parent PWB!'.format(chip.code))
            if parent.dump['componentType']['code'] == 'AMAC_GELPACK':
                print(' Found AMACv2 ({}) still on AMAC_GELPACK ({}), disassemble it from AMAC_GELPACK now'.format(chip.code, parent.code))
                parent.disassemble(chip)
                time.sleep(2)

    print('Found an AMACv2: {}'.format(chip.code))    

    # Assemble :)
    print('Assembling to Powerboard {}'.format(pwb.code))
    pwb.assemble(chip)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Manually load a Powerboard.")

    parser.add_argument("panelid"           , help="Panel identifier (VBBXXXX).")
    parser.add_argument("position", type=int, help="Powerboard position.")
    parser.add_argument("wafer"   , type=str, help="AMAC wafer ID.")
    parser.add_argument("waferpos", type=int, help="AMAC wafer position.")
    parser.add_argument("-s", "--atlasSN", default="", help="ATLAS SN of the AMAC.")

    args = parser.parse_args()

    c = pwbdbtools.get_db_client()

    loadwafer(c, args.panelid, args.position, args.wafer, args.waferpos, args.atlasSN)
