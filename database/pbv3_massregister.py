#!/usr/bin/env python

import os, sys
import argparse
import itkdb

from database import pwbdbtools

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Mass register components without any identifiers.")

    parser.add_argument("-b","--batch"       , help="Batch in format BATCH_CODE:BATCH_NUMBER.")    
    parser.add_argument("subproject"         , help="Subproject identifier (SG, SB or SE).")
    parser.add_argument("component"          , help="Component type.")
    parser.add_argument("type"               , help="Component subtype.")
    parser.add_argument("count"    , type=int, help="Number of components to register.")

    args = parser.parse_args()

    regdata={
        'project':'S',
        'subproject':args.subproject,
        'institution':'LBNL_STRIP_POWERBOARDS',
        'componentType':args.component,
        'type':args.type,
        }
    if args.batch!=None:
        parts=args.batch.split(':')
        if len(parts)<2:
            print('Invalid batch specification. Must be in format `BATCH_CODE:BATCH_NUMBER`!')
            sys.exit(1)
        batch_code=parts[0]
        batch_numb=':'.join(parts[1:])
        regdata['batches']={batch_code:batch_numb}
    
    c = pwbdbtools.get_db_client()

    print("Registering {}...".format(args.component))
    for i in range(args.count):
        c.post("registerComponent", json = regdata)
        print('{} done'.format(i))
