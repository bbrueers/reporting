#!/usr/bin/env python3

import os
import argparse
import itkdb
import time
import json
import getpass

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Compare time to run getComponent with serial Number version component id.")

    parser.add_argument("component", help="Code of component.")
    parser.add_argument("type", help="Code for component subtype.")

    args = parser.parse_args()

    if os.environ.get('ITKDB_ACCESS_CODE1') and os.environ.get('ITKDB_ACCESS_CODE2'):
        accessCode1 = os.environ.get('ITKDB_ACCESS_CODE1')
        accessCode2 = os.environ.get('ITKDB_ACCESS_CODE2')
    else:
        accessCode1 = getpass.getpass("AccessCode1: ")
        accessCode2 = getpass.getpass("AccessCode2: ")
  
    u = itkdb.core.User(accessCode1 = accessCode1, accessCode2 = accessCode2)
    c = itkdb.Client(user = u, expires_after=dict(days=1))
    c.user.authenticate()

    #Get all components using listComponents
    listComp = c.get("listComponents", json = {'project':'S', 'componentType':args.component, 'type':args.type})

    code = []
    serial = []
    registration = []

    #loop through components returned by listComponents
    for component in listComp:
        now = time.time()
        c.get("getComponent", json = {'component':component['code']})
        code.append(time.time()-now)

    #Must get all components again using listComponents as generator has been run through
    listComp = c.get("listComponents", json = {'project':'S', 'componentType':args.component, 'type':args.type})

    #loop through components returned by listComponents
    for component in listComp:
        now = time.time()
        getComp = c.get("getComponent", json = {'component':component['serialNumber']})
        serial.append(time.time()-now)
        registration.append(getComp['cts'])

    data = {}
    data['component'] = args.component
    data['type'] = args.type
    data['code'] = code
    data['serial'] = serial
    data['registration'] = registration

    with open('data/getComponent_serial_vs_code.json', 'w+') as f:
        json.dump(data, f)
