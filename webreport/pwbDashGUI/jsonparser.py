import pandas as pd
import json
import os

import glob
 
 
class JsonParser():
    def __init__(self, datadir="", num_of_boards=10):
        self.datadir = datadir
        self.panelName = ""
        self.version = None
        self.batch = None
        self.boards = [-1] * num_of_boards
        self.serials = [-1] * num_of_boards
        self.json_files = [["" for _ in range(3)] for _ in range(num_of_boards)]
        self.nPB_selected = num_of_boards
    def updatePanel(self, datadir, panel, version, batch, boards, serials, label):
        self.datadir = datadir
        self.panelName = panel
        self.version = version
        self.boards = [-1] * len (self.boards)
        self.serials = [-1] * len (self.boards)
        self.json_files = [["" for _ in row] for row in self.json_files]
        self.nPB_selected = len(serials)
        self.boards[:len(boards)] = boards
        self.serials[:len(serials)] = serials

        print("Getting test result files for the following PBs (position idx):")
        print(boards)

        ### find the latest test result json files for the correspponding label
        result_dir = self.datadir
        result_dir += "/panel"+self.panelName
        subdirs = glob.glob(result_dir+"/*")

        if len(subdirs) == 0:
            print("No file to display!")
            return "No file to display!"
        
        for ipb in range(self.nPB_selected):
            fileTypes = ["alive.json", "functionality.json", "advanced.json"]
            for idx_fileType in range(len(fileTypes)):
                fileType = fileTypes[idx_fileType]
                filesAll = glob.glob(result_dir+"/*/pb20USBP0{:01d}{:02d}{:04d}".format(version, batch, int(serials[ipb]))+"/*"+fileType)
                if len(filesAll) == 0:
                    continue
                filesAll_sorted = sorted(filesAll, key = os.path.getmtime)
                for file_json in reversed(filesAll_sorted):
                    if os.path.getsize(file_json) < 10: #ignore empty file
                        continue
                    if label != "":
                        jdata = json.load(open(file_json))
                        if "testTag" in jdata and jdata["testTag"] == label:
                            self.json_files[int(boards[ipb])][idx_fileType] = file_json
                            print("Display file for P"+str(boards[ipb])+": "+file_json)
                            break
                    else:
                        self.json_files[int(boards[ipb])][idx_fileType] = file_json
                        print("Display file for P"+str(boards[ipb])+": "+file_json)
                        break
       
        count_non_empty = sum(1 for row in self.json_files for item in row if item != "")
        rows_with_non_empty = sum(1 for row in self.json_files if any(item != "" for item in row))

        if count_non_empty == 0:
            print("No file to display!")
            return "No file to display!"

        return "Displaying "+str(count_non_empty)+" json files for "+str(rows_with_non_empty)+" PBs in total."

    def get_test_results(self, testType, listOfVars, category=0, OnOff=[], get_array=False, noError=False):
        df = pd.DataFrame()

        for pb in self.boards:
            if pb == -1:
                continue
            if self.json_files[pb][category] == "":
                continue
            if get_array:
                df_temp = self.get_test_result_array(self.json_files[pb][category], testType, listOfVars, pb, noError)
                df = pd.concat([df, df_temp], ignore_index=True)
            else:
                df_temp = self.get_test_result(self.json_files[pb][category], testType, listOfVars, pb, OnOff, noError)
                df = pd.concat([df, df_temp], ignore_index=True)
        return df
        
    def get_test_result(self, path, testType, listOfVars, pb=0, OnOff=[], noError=False):
        jdata = json.load(open(path))
        df = pd.DataFrame(data={'pb':[pb]})
        value = None
        error = None
        for test in jdata['tests']:
            if test['testType'] == testType:
                error = int(test['passed'] == False)
                for cut in listOfVars:
                    key = cut[0]
                    value = test['results'][key]
                    if len(OnOff) == 2 and len(value) == 2:
                        value_off = value[OnOff[1]]
                        value_on = value[OnOff[0]]
                        error_off = value_off < cut[1][2] or value_off > cut[1][3]
                        error_on = value_on < cut[1][0] or value_on > cut[1][1]
                        df['value_'+key+'ON'] = [value_on]
                        df['value_'+key+'OFF'] = [value_off]
                        if not noError:
                            df['error_'+key+'ON'] = [int(error_off)]
                            df['error_'+key+'OFF'] = [int(error_on)]
                    else:
                        df['value_'+key] = [value]
                        if not noError:
                            df['error_'+key] = [int(value < cut[1][0] or value > cut[1][1])]
                break
        if len(df.columns) == 1:
            df = pd.DataFrame()
        return df

    def get_test_result_array(self, path, testType, listOfVars, pb=0, noError=False):
        jdata = json.load(open(path))
        df = pd.DataFrame(data={'pb':[pb]})
        value = None
        error = None
        for test in jdata['tests']:
            if test['testType'] == testType:
                error = int(test['passed'] == False)
                for key in listOfVars:
                    value = [item for item in test['results'][key] if item is not None]
                    if not noError:
                        df['error_'+key] = [int(error)]
                    df['value_'+key] = [value]
                break
        if len(df.columns) == 1:
            df = pd.DataFrame()
        return df
