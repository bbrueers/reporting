import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_table
from dash_table.Format import Format, Scheme, Sign, Symbol
from dash.dependencies import Input, Output, State, ClientsideFunction
from dash.exceptions import PreventUpdate

import plotly.graph_objects as go
from time import time, sleep
import codecs
import pickle
import datetime
import pkg_resources

import plotly.graph_objects as go
import pandas as pd
import glob

from webreport.pwbDashCommon import template

from . import panel

from database import pwbdb
import os
import sys
from io import StringIO

import itkdb
import datetime

class Capturing(list):
    def __enter__(self):
        self._stdout = sys.stdout
        sys.stdout = self._stringio = StringIO()
        return self
    def __exit__(self, *args):
        self.extend(self._stringio.getvalue().splitlines())
        del self._stringio    # free up some memory
        sys.stdout = self._stdout

def parseContent(inString, keyword):
    key1 = keyword + ':---> '
    key2 = ' <---:'+keyword
    idx_begin = inString.rfind(key1)
    idx_end = inString.rfind(key2)
    if idx_begin == -1 or idx_end == -1:
        return ""
    return inString[idx_begin+len(key1):idx_end]

def pad_dataframe_pbs(df):
    """
    Fill a dataframe with powerboards that don't exist in it.
    """
    allpbs=set(range(0,10))
    pbs=set(df.pb.unique())
    misspbs=allpbs-pbs
    return df.append([{'pb': pb} for pb in misspbs]).sort_values('pb')


# A function to expand the rows of the DataFrame.
def dataframe_expand_rows(row, columns, reset_row):
    # Get the length of the first list (assuming all lists in a row have the same length).
    length = len(next(cell for cell in row if isinstance(cell, list)))
    # Replicate the reset_row value based on the list's length.
    pb_series = [row[reset_row]] * length

    # Extract individual values for each column, excluding 'pb'.
    col_data = {col: row[col] for col in columns if col != reset_row}

    # Return a DataFrame with expanded rows.
    return pd.DataFrame({**{reset_row: pb_series}, **col_data})

def get_dataframe_DAC(panel, testType, vars_dac):
    vars_rename = ["DAC", "External", "Internal"]
    panel_dac_data = panel.jsonparser.get_test_results(testType, vars_dac, category=2, get_array=True, noError=True)
    if panel_dac_data.empty:
        return panel_dac_data

    for idx in range(len(vars_dac)):
        panel_dac_data.rename(columns=lambda x: x.replace('value_'+vars_dac[idx], vars_rename[idx]), inplace=True)

    panel_dac_data = pd.concat([dataframe_expand_rows(row, panel_dac_data.columns, 'pb') for _, row in panel_dac_data.iterrows()]).reset_index(drop=True)

    return panel_dac_data


def pivot_ven_dataframe(df, oncol, invert=False):
    """
    Rotates a dataframe from a VEnParser into a table
    format.

    df: Original dataframe
    oncol: Name of column indicating on/off state
    invert: oncol==0 means ON
    """
    df=df.copy()
    str0 = "OFF" if not invert else "ON"
    str1 = "ON"  if not invert else "OFF"
        
    df['suffix']=df.apply(lambda x: str1 if x[oncol] else str0, axis=1, result_type='reduce')
    df=df.set_index(['pb','suffix']).unstack(-1)
    df.columns=df.columns.map(''.join)
    return df.reset_index()

class PBv3TableGen:
    def __init__(self, tableid):
        self.tableid=tableid

        self.coldef  =[{'name':['','Powerboard'],'id':'pb'}]
        self.condform=[{'if':{'column_id':'pb'},'width':'50px'}]        

    def add_columns(self,columns):
        for column in columns:
            if type(column)!=dict:
                column={
                    'id': column,
                    'name': column
                    }
            if type(column["name"])!=tuple:
                name=('',column["name"])
            else:
                name=column['name']
                

            self.coldef+=[
                {'name':name,
                     'id':f'value_{column["id"]}',
                     'type':'numeric',
                     'format': Format(
                         precision=column.get('precision',2),
                         scheme=column.get('scheme',Scheme.fixed))
                }
            ]

            self.condform+=[
                {
                    'if': {
                        'column_id': f'value_{column["id"]}',
                        'filter_query': f'{{error_{column["id"]}}} = 0'
                        },
                        'backgroundColor': 'lightgreen'
                },
                {
                    'if': {
                        'column_id': f'value_{column["id"]}',
                        'filter_query': f'{{error_{column["id"]}}} = 1'
                        },
                        'backgroundColor': 'salmon'
                }
                ]

    def add_onoff_columns(self,columns):
        for column in columns:
            if type(column)!=dict:
                column={
                    'id': column,
                    'name': column
                    }
            
            self.coldef+=[
                {'name':[f'{column["name"]} [{column.get("units","V")}]','OFF'],
                     'id':f'value_{column["id"]}OFF',
                     'type':'numeric',
                     'format': Format(
                         precision=2,
                         scheme=column.get('scheme',Scheme.fixed))
                },
                {'name':[f'{column["name"]} [{column.get("units","V")}]','ON'],
                     'id':f'value_{column["id"]}ON',
                     'type':'numeric',
                     'format': Format(
                         precision=2,
                         scheme=column.get('scheme',Scheme.fixed))
                }
                ]

            self.condform+=[
                {
                    'if': {
                        'column_id': f'value_{column["id"]}OFF',
                        'filter_query': f'{{error_{column["id"]}OFF}} = 0'
                        },
                        'backgroundColor': 'lightgreen'
                },
                {
                    'if': {
                        'column_id': f'value_{column["id"]}OFF',
                        'filter_query': f'{{error_{column["id"]}OFF}} = 1'
                        },
                        'backgroundColor': 'salmon'
                },
                {
                    'if': {
                        'column_id': f'value_{column["id"]}ON',
                        'filter_query': f'{{error_{column["id"]}ON}} = 0'
                        },
                        'backgroundColor': 'lightgreen'
                },
                {
                    'if': {
                        'column_id': f'value_{column["id"]}ON',
                        'filter_query': f'{{error_{column["id"]}ON}} = 1'
                        },
                        'backgroundColor': 'salmon'
                },
                ]

    def generate(self):
        return dash_table.DataTable(
            id=self.tableid,
            merge_duplicate_headers=True,
            columns=self.coldef,
            style_data_conditional=self.condform,
            style_header={'fontWeight': 'bold'},
            style_cell={'width':'85px'}
            )            

class DisplayPage:
    def __init__(self, cfg, app):
        #
        # Run stuff
        self.cfg=cfg

        self.panels=[]
        panellabels=[]
        for panelcfg in cfg.panels:
            panelcfg=panelcfg.copy()
            name  =panelcfg.pop('name'  )
            runner=panelcfg.pop('runner')

            panelobj=getattr(panel,runner)(**panelcfg)

            self.panels.append(panelobj)
            panellabels.append(name)

        #
        # Layout related stuff
        pbchecks=[
            dcc.Checklist(id=f'pb-{pb}-display',
                          options = [{'label': 'Board {}'.format(pb), 'value': pb}])
            for pb in range(10)]

        rundropdowns=[
            dcc.Dropdown(id='transition-dropdown-display', options = [
            {'label': 'SMD Loading', 'value': 'SMD_LOAD'},
            {'label': 'Coil and Shield Loading', 'value': 'MAN_LOAD'},
            {'label': 'Die Attachment and Bonding', 'value': 'BONDED'},
            {'label': 'Thermal Cycling', 'value': 'THERMAL'},
            {'label': 'Burn-In', 'value': 'BURN_IN'},
            {'label': 'Module Reception',   'value': 'MODULE_RCP'},
            {'label': 'Loaded on a Module', 'value': 'LOADED'},
            {'label': 'Loaded on a Hybrid Burn-in Carrier', 'value': 'HYBBURN'}
            ], value='MODULE_RCP'),
            dcc.Dropdown(id='display-label-dropdown', options = [
            {'label': 'BONDED warm', 'value': 'BONDED warm'},
            {'label': 'BONDED cold', 'value': 'BONDED cold'},
            {'label': 'THERMAL warm', 'value': 'THERMAL warm'},
            {'label': 'THERMAL cold', 'value': 'THERMAL cold'},
            {'label': 'BURN_IN warm', 'value': 'BURN_IN warm'},
            {'label': 'BURN_IN cold', 'value': 'BURN_IN cold'}
            ], value='', placeholder="Select a result tag")
            ]

        transitionchecks=[
            dcc.Checklist(id='force-transition-display', options = [
            {'label' : 'force', 'value' : 1}
            ], value = [1])
            ]


        # Tables
        table_ven  = PBv3TableGen('LVEn-table-display')
        table_ven.add_columns([
            {'id':'PADID','name':'Pad ID','precision':0},
            {'id':'RELIABILITY','name':'BER','precision':2},
            ])
        table_ven.add_onoff_columns([
            'linPOLV',
            {'id':'VOUT','name':'DC/DC Out'},
            {'id':'HVIIN'    , 'name':'HV In Current' ,'units':'A','precision':2,'scheme':Scheme.decimal_or_exponent},
            {'id':'HVIOUT'   , 'name':'HV Out Current','units':'A','precision':2,'scheme':Scheme.decimal_or_exponent},
            {'id':'AMACHVRET', 'name':'HVret'         ,'units':'counts','precision':0},            
            ])

        table_tio1 = PBv3TableGen('tio-table-1-display')
        table_tio1.add_onoff_columns(['OFout', 'CALx', 'CALy', 'Shuntx', 'Shunty'])

        table_tio2 = PBv3TableGen('tio-table-2-display')
        table_tio2.add_onoff_columns(['LDx0EN', 'LDx1EN', 'LDx2EN', 'LDy0EN', 'LDy1EN', 'LDy2EN'])

        table_temp = PBv3TableGen('temp-table-display')
        table_temp.add_columns([
            {'id':'NTCx' ,'name':'NTCx [cnt]' ,'precision':0},
            {'id':'NTCy' ,'name':'NTCy [cnt]' ,'precision':0},
            {'id':'NTCpb','name':'NTCpb [cnt]','precision':0},
            {'id':'CTAT' ,'name':'CTAT [cnt]' ,'precision':0},
            {'id':'PTAT' ,'name':'PTAT [cnt]' ,'precision':0},
        ])

        table_dcdcadj = PBv3TableGen('dcdcadj-table-display')
        table_dcdcadj.add_columns([
            {'id':'minusThirteen' ,'name':'-13%','precision':1},
            {'id':'minusSix'      ,'name':'-6%' ,'precision':1},
            {'id':'plusSix'       ,'name':'+6%' ,'precision':1},
        ])
        
        table_dcdc = PBv3TableGen('eff-table-display')
        table_dcdc.add_columns([
            {'id':'EFF2A','name':('2A Load','Efficiency'),'precision':2},
            {'id':'AMACPTAT2A','name':('2A Load','AMAC NTC PB [counts]'),'precision':0}
            ])

        tables=[
            table_ven    .generate(),
            table_tio1   .generate(),
            table_tio2   .generate(),
            table_temp   .generate(),
            table_dcdcadj.generate(),
            table_dcdc   .generate()
            ]

        # Create the layout
        self.layout = template.jinja2_to_dash(
            pkg_resources.resource_filename(__name__,'template'),
            'display.html', replace=pbchecks+transitionchecks+rundropdowns+tables, panels=panellabels)

        #
        # Callbacks

        ## Server-side Callbacks
        app.callback(
            [
                Output('panel-dbresult-display', 'data'),
                Output('auto-fill-msg-display', 'children'),
            ],
            Input('panel-populate-button-display', 'n_clicks'),
            [
                State('panel-number-display', 'value'),
                State('itkdb_auth', 'data'),
                State('itkdb_auth_pass1', 'data'),
                State('itkdb_auth_pass2', 'data')
            ],
            prevent_initial_call=True
            )(self.populateFromDB)
 
        app.callback(
            Output('upload-test-msg-display', 'children'),
            [Input('panel-upload-test-result-button-display', 'n_clicks')],
            [
                State('itkdb_auth', 'data'),
                State('tabsDisplay', 'value'),
                State('itkdb_auth_pass1', 'data'),
                State('itkdb_auth_pass2', 'data'),
                State('panel-number-display', 'value'),
                State('version-display', 'value'),
                State('batch-display', 'value'),
                State('pb-0-display', 'value'),
                State('pb-1-display', 'value'),
                State('pb-2-display', 'value'),
                State('pb-3-display', 'value'),
                State('pb-4-display', 'value'),
                State('pb-5-display', 'value'),
                State('pb-6-display', 'value'),
                State('pb-7-display', 'value'),
                State('pb-8-display', 'value'),
                State('pb-9-display', 'value'),
                State('serial-0-display', 'value'),
                State('serial-1-display', 'value'),
                State('serial-2-display', 'value'),
                State('serial-3-display', 'value'),
                State('serial-4-display', 'value'),
                State('serial-5-display', 'value'),
                State('serial-6-display', 'value'),
                State('serial-7-display', 'value'),
                State('serial-8-display', 'value'),
                State('serial-9-display', 'value')
            ]
            )(self.uploadTestResults)

        app.callback(
            Output('panel-transition-msg-display', 'children'),
            [Input('panel-transition-button-display', 'n_clicks')],
            [
                State('transition-dropdown-display', 'value'),
                State('force-transition-display', 'value'),
                State('itkdb_auth', 'data'),
                State('tabsDisplay', 'value'),
                State('itkdb_auth_pass1', 'data'),
                State('itkdb_auth_pass2', 'data'),
                State('panel-number-display', 'value')
            ]
            )(self.panelStageTransition)
 
        app.callback(
            Output('display-test-result-msg', 'children'),
            [Input('display-test-result-button', 'n_clicks')],
            [
                State('display-label-dropdown', 'value'),
                State('display-label-custom', 'value'),
                State('tabsDisplay', 'value'),
                State('panel-number-display', 'value'),
                State('version-display', 'value'),
                State('batch-display', 'value'),
                State('pb-0-display', 'value'),
                State('pb-1-display', 'value'),
                State('pb-2-display', 'value'),
                State('pb-3-display', 'value'),
                State('pb-4-display', 'value'),
                State('pb-5-display', 'value'),
                State('pb-6-display', 'value'),
                State('pb-7-display', 'value'),
                State('pb-8-display', 'value'),
                State('pb-9-display', 'value'),
                State('serial-0-display', 'value'),
                State('serial-1-display', 'value'),
                State('serial-2-display', 'value'),
                State('serial-3-display', 'value'),
                State('serial-4-display', 'value'),
                State('serial-5-display', 'value'),
                State('serial-6-display', 'value'),
                State('serial-7-display', 'value'),
                State('serial-8-display', 'value'),
                State('serial-9-display', 'value')
            ]
            )(self.displayTestResults)

        app.callback(
            Output('upload-status-msg-display', 'children'),
            [Input('interval-component-run-display', 'n_intervals'), Input('tabsDisplay','value')],
        )(self.updateUploadTestResultsStatus)

        app.callback(
            Output('transition-status-msg-display', 'children'),
            [Input('interval-component-run-display', 'n_intervals'), Input('tabsDisplay','value')],
        )(self.updateTransitionStatus)
      
       
        app.callback(
            [
                Output('display-test-display', 'children'),
            ],
            [Input('tabsDisplay','value'), Input('interval-component-run-display', 'n_intervals')]
        )(self.updateRunData)
        

        app.callback(
            [
                Output('LVEn-table-display', 'data'),
            ],
            [Input('interval-component-run-display', 'n_intervals')], 
            [State('tabsDisplay','value')]
        )(self.updateVEndata)

        
        app.callback(
            [
                Output('tio-data-display', 'data'),
            ],
            [Input('interval-component-run-display', 'n_intervals')], 
            [State('tabsDisplay','value')]
        )(self.updateTIOData)


        app.callback(
            [
                Output('temp-table-display', 'data'),
            ],
            [Input('interval-component-run-display', 'n_intervals')], 
            [State('tabsDisplay','value')]
        )(self.updateTempData)

        app.callback(
            [
                Output('dcdcadj-table-display', 'data'),
            ],
            [Input('interval-component-run-display', 'n_intervals')], 
            [State('tabsDisplay','value')]
        )(self.updateDCDCAdjData)        

        app.callback(
            [
                Output('lviv-data-display', 'data'),
            ],
            [Input('interval-component-run-display', 'n_intervals')], 
            [State('tabsDisplay','value')]
        )(self.updateLVIVdata)

        app.callback(
            [
                Output('amsl-data-display', 'data'),
            ],
            [Input('interval-component-run-display', 'n_intervals')],
            [State('tabsDisplay','value')]
        )(self.updateAMSlopedata)

        app.callback(
            [
                Output('dac-data-display', 'data'),
            ],
            [Input('interval-component-run-display', 'n_intervals')], 
            [State('tabsDisplay','value')]
        )(self.updateDACdata)

        app.callback(
            [
                Output('eff-table-display', 'data'),
                Output('dcdc-data-display', 'data'),
            ],
            [Input('interval-component-run-display', 'n_intervals')], 
            [State('tabsDisplay','value')]
        )(self.updateDCDCdata)

        app.callback(
            [
                Output('hvrt-data-display', 'data'),
            ],
            [Input('interval-component-run-display', 'n_intervals')], 
            [State('tabsDisplay','value')]
        )(self.updateHVdata)

        app.callback(
            Output('display-status', 'data'),
            [Input('interval-component-status-display', 'n_intervals')]
        )(self.updateRunStatus)


        ## Client-side Callbacks

        # Store checkbox settings per panel
        displaystoreelements=['panel-number-display', 'version-display', 'batch-display']
        displaystoreelements+=[f'pb-{pb}-display' for pb in range(10)]
        displaystoreelements+=[f'serial-{pb}-display' for pb in range(10)]

        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='storeDisplayInfo'
                ),
            Output('display-storage', 'data'),
            [Input(name, 'value') for name in displaystoreelements],
            [State('display-storage', 'data'), State('tabsDisplay','value')])

        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='restoreDisplayInfo'
                ),
            [Output(name, 'value') for name in displaystoreelements],
            [Input('tabsDisplay', 'value'), Input('panel-dbresult-display', 'data')],
            [State('display-storage', 'data')])
        
        # Update status styles
        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='updateTabColors'
                ),
            [o for p in range(len(self.panels)) for o in [Output(f'tabDisplay-{p}', 'style'),Output(f'tabDisplay-{p}', 'selected_style')]],
            [Input('display-status', 'data')],
            prevent_initial_call=True
            )

        # Graph updates
        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='updateTIOtable'
                ),
                [
                    Output('tio-table-1-display', 'data'),
                    Output('tio-table-2-display', 'data')
                ],
                [Input('tio-data-display', 'data')]
            )

        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='updateLVIVgraph'
                ),
                Output('lviv-graph-display','figure'),
                [Input('lviv-data-display', 'data')]
            )

        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='updateAMSlopegraph'
                ),
                Output('amsl-graph-display','figure'),
                [Input('amsl-data-display', 'data')]
            )

        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='updateDACgraph'
                ),
                [
                    Output('shuntx-graph-display','figure'),
                    Output('shunty-graph-display','figure'),
                    Output('calx-graph-display','figure'),
                    Output('caly-graph-display','figure')
                ],
                [Input('dac-data-display', 'data')]
            )

        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='updateDCDCgraph'
                ),
                [
                    Output('eff-graph-display','figure'),
                    Output('temp-graph-display','figure'),
                    Output('icur-graph-display','figure'),
                    Output('ocur-graph-display','figure')
                ],
                [Input('dcdc-data-display', 'data')]
            )

        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='updateHVretgraph'
                ),
                [
                    Output('gain0-graph-display','figure'),
                    Output('gain1-graph-display','figure'),
                    Output('gain2-graph-display','figure'),
                    Output('gain4-graph-display','figure')
                ],
                [Input('hvrt-data-display', 'data')]
            )

    def populateFromDB(self, button, panel, user, pass1, pass2):
        """
        Query the database for Powerboard ID's on a panel and return
        the values in a dictionary.

        If an error occurs (invalid login, mixing version/batch numbers),
        then an exception is thrown.

        Dictionary Keys:
         - version: Version number
         - batch: Batch number
         - pbEnable: List of 10 booleans, true if the Powerboard is preset
         - pbNumber: List of 10 integers, corresponding to Powerboard number
        """
        if panel == '':
            return None, ' Please provide a panel number'
        if user==None:
            return None, ' Please login to ITk DB first!!!'

        # Create client
        user=pickle.loads(codecs.decode(user.encode(),'base64'))

        if (not user.is_authenticated()) or (pass1 is None) or (pass2 is None):
            return None, ' Please login to ITk DB first!!!'
        if user.is_expired() or user.expires_in < 300:
            print('IDk DB login is expired or is expiring, re-login')
            user = itkdb.core.User(pass1, pass2)
            user.authenticate()
        if not user.is_authenticated():
            return None, ' Please login to ITk DB first!!!'

        client=itkdb.Client(user=user)

        #
        # Query the database
        result={'pbEnable':[False]*10, 'pbNumber':[0]*10}
        carrier=pwbdb.Component(f'20USBPC{panel}', client)
        componentType = ""
        try:
            componentType = carrier.componentType
        except:
            componentType = "InValid"
        if componentType != "PWB_CARRIER":
            return None, ' Panel not found in DB. Please provide a valid panel number!!!'

        for pbNum,pb in carrier.children.get('PWB',{}).items():
            # Update common values
            if result.get('version', pb.property('VERSION'))!=pb.property('VERSION'):
                return None, ' Cannot mix version numbers'
            result['version']=pb.property('VERSION')

            if result.get('batch', pb.property('BATCH'))!=pb.property('BATCH'):
                return None, ' Cannot mix batch numbers'
            result['batch']=pb.property('BATCH')

            # Enable powerboard
            result['pbEnable'][pbNum]=True
            result['pbNumber'][pbNum]=pb.property('PB_NUMBWE')

        return result, ' '

    def displayTestResults(self, button, label_dropdown, label_custom, tab, *args):

        if button==None:
            raise PreventUpdate

        label = ""

        if label_custom != "":
            label = label_custom
        elif label_dropdown is not None:
            label = label_dropdown
        else:
            label = ""
        label = label.replace(" ", "_")

        panel  =args[0].strip()
        if panel == '':
            return ' Please provide a panel number!'

        if args[1] == '':
            return ' Please provide a powerboard version number!'
        version=int(args[1])

        if args[2] == '':
            return ' Please provide a powerboard batch number!'
        batch  =int(args[2])

        boards =sum(args[3:13],[])
        serials=map(args[13:23].__getitem__, boards)

        if len(boards)==0:
            return 'No boards selected!'

        self.panels[int(tab)].data=[]
        for parser in self.panels[int(tab)].dbparser.parsers:
            parser.clear()

        datadir = self.panels[int(tab)].datadir
        if self.panels[int(tab)].datadir_local != None:
            datadir = self.panels[int(tab)].datadir_local
        with Capturing() as output:
            result_update_panel = self.panels[int(tab)].jsonparser.updatePanel(datadir, panel, version, batch, list(boards), list(serials), label)
        self.panels[int(tab)].parseOutput(output)

        self.panels[int(tab)].refreshDisplayResult_VEn = True
        self.panels[int(tab)].refreshDisplayResult_TIO = True
        self.panels[int(tab)].refreshDisplayResult_Temp = True
        self.panels[int(tab)].refreshDisplayResult_DCDCAdj = True
        self.panels[int(tab)].refreshDisplayResult_AMSlope = True
        self.panels[int(tab)].refreshDisplayResult_DCDCdata = True
        self.panels[int(tab)].refreshDisplayResult_DACdata = True
        self.panels[int(tab)].refreshDisplayResult_HVdata = True
        self.panels[int(tab)].refreshDisplayResult_LVIVdata = True


        if label != "":
            return 'Display result for tag = "'+label+'" below... '+result_update_panel
        else:
            return 'No result tag provided, displaying latest result below...'+result_update_panel

    def uploadTestResults(self, button, user, tab, pass1, pass2, *args):
        """
        Upload test results to idkdb
        """
        if button==None:
            raise PreventUpdate

        panel  =args[0].strip()
        if panel == '':
            return ' Please provide a panel number!'

        if args[1] == '':
            return ' Please provide a powerboard version number!'
        version=int(args[1])

        if args[2] == '':
            return ' Please provide a powerboard batch number!'
        batch  =int(args[2])

        boards =sum(args[3:13],[])
        serials=map(args[13:23].__getitem__, boards)

        if len(boards)==0:
            return 'No boards selected!'

        if user==None:
            return ' Please login to ITk DB first!!!'

        # Create client
        user=pickle.loads(codecs.decode(user.encode(),'base64'))

        if (not user.is_authenticated()) or (pass1 is None) or (pass2 is None):
            return ' Please login to ITk DB first!!!'
        if user.is_expired() or user.expires_in < 300:
            print('IDk DB login is expired or is expiring, re-login')
            user = itkdb.core.User(pass1, pass2)
            user.authenticate()
        if not user.is_authenticated():
            return ' Please login to ITk DB first!!!'

        client=itkdb.Client(user=user)

        msg = ""
        try:
            msg = self.panels[int(tab)].runUploadData(client, version, batch, boards, serials, panelName=panel)
        except:
            return sys.exc_info()[0]
        
        return msg

    def panelStageTransition(self, button, stage, force, user, tab, pass1, pass2, *args):
        """
        Upload test results to idkdb
        """
        if button==None:
            raise PreventUpdate

        panel  =args[0].strip()

        if panel == '':
            return ' Please provide a panel number!'
        if stage == None:
            return ' Please select a stage to transition to!'

        if user==None:
            return ' Please login to ITk DB first!!!'

        # Create client
        user=pickle.loads(codecs.decode(user.encode(),'base64'))

        if (not user.is_authenticated()) or (pass1 is None) or (pass2 is None):
            return ' Please login to ITk DB first!!!'
        if user.is_expired() or user.expires_in < 300:
            print('IDk DB login is expired or is expiring, re-login')
            user = itkdb.core.User(pass1, pass2)
            user.authenticate()
        if not user.is_authenticated():
            return ' Please login to ITk DB first!!!'

        client=itkdb.Client(user=user)

        force_ = False
        if len(force) > 0:
            force_ = True

        msg = ""

        try:
            msg = self.panels[int(tab)].runPanelTransition(client, panel, stage, force_)
        except:
            return sys.exc_info()[0]

        return msg

   
    def updateRunStatus(self, interval):
        return [ panel.running for panel in self.panels]
        #return [ panel.running for panel in self.panels]

    def updateRunData(self, tab, interval):
        # Parse new data
        start=time()
        panel=self.panels[int(tab)]
        panel.parse()

        # Terminal output
        terminal = '\n'.join(panel.data)

        return [terminal]

    def updateTempData(self, interval, tab):
        # Parse new data
        panel=self.panels[int(tab)]        

        if panel.refreshDisplayResult_Temp == False:
            raise PreventUpdate

        panel.refreshDisplayResult_Temp = False

        #
        # Empty dataframe with data
        tbl = pd.DataFrame(data={'pb':range(10)})        

        # Temperature
        ## first, determine if this is a cold test
        isColdTest = False
        df_range = panel.jsonparser.get_test_results("TEMPERATURE", [("NTCpbSenseRange", [-1, 2000])], category=1)
        if not df_range.empty:
            value_NTCpbSenseRange = df_range.loc[0, 'value_NTCpbSenseRange']
            if value_NTCpbSenseRange == 7:
                isColdTest = True
        # different cuts for cold and warm tests
        cuts_temp = [
            ("AMACCTAT", [200 if isColdTest else 200, 600 if isColdTest else 600]),
            ("AMACPTAT", [500 if isColdTest else 500, 900 if isColdTest else 900]),
            ("AMACNTCPB", [500 if isColdTest else 600, 1024 if isColdTest else 1024]),
            ("AMACNTCX", [550 if isColdTest else 600, 850 if isColdTest else 850]),
            ("AMACNTCY", [550 if isColdTest else 600, 850 if isColdTest else 850])
        ]
        
        # get the data
        panel_temp_data = panel.jsonparser.get_test_results("TEMPERATURE", cuts_temp, category=1)
        panel_temp_data.rename(columns=lambda x: x.replace('AMAC', ''), inplace=True)
        panel_temp_data.rename(columns=lambda x: x.replace('NTCPB', 'NTCpb'), inplace=True)
        panel_temp_data.rename(columns=lambda x: x.replace('NTCX', 'NTCx'), inplace=True)
        panel_temp_data.rename(columns=lambda x: x.replace('NTCY', 'NTCy'), inplace=True)
        if not panel_temp_data.empty:
            tbl=tbl.merge(panel_temp_data, on='pb', how='outer')

        # Final table
        tbl=tbl.to_dict('records')

        return [tbl]

    def updateDCDCAdjData(self, interval, tab):
        # Parse new data
        panel=self.panels[int(tab)]        

        if panel.refreshDisplayResult_DCDCAdj == False:
            raise PreventUpdate

        panel.refreshDisplayResult_DCDCAdj = False


        #
        # Empty dataframe with data
        tbl = pd.DataFrame(data={'pb':range(10)})        

        # DCDC Adjust        
        # first, get the VOUT array
        vout_array = panel.jsonparser.get_test_results("DCDC_ADJUST", ["VOUT"], category=1, get_array=True)

        panel_dcad_data = pd.DataFrame()
        for irow in range(len(vout_array)):
            pb = vout_array.loc[irow, 'pb']
            df = pd.DataFrame(data={'pb':[pb]})

            shifts = vout_array.loc[irow, 'value_VOUT']
            minusSix = (shifts[1] - shifts[0]) / shifts[0]
            minusThirteen = (shifts[2] - shifts[0]) / shifts[0]
            plusSix = (shifts[3] - shifts[0]) / shifts[0]
            minusSixError = int(not (-.0767 < minusSix and  minusSix < -.0567))
            minusThirteenError = int(not (-.143 < minusThirteen and minusThirteen < -.123))
            plusSixError = int(not (.0567 < plusSix and plusSix < .0767))

            df['value_minusSix'] = [minusSix]
            df['error_minusSix'] = [minusSixError]
            df['value_minusThirteen'] = [minusThirteen]
            df['error_minusThirteen'] = [minusThirteenError]
            df['value_plusSix'] = [plusSix]
            df['error_plusSix'] = [plusSixError]

            panel_dcad_data = pd.concat([panel_dcad_data, df], ignore_index=True)

        if not panel_dcad_data.empty:
            tbl=tbl.merge(panel_dcad_data, on='pb', how='outer')
        # Turn into %
        for column in tbl.columns:
            if not column.startswith('value'):
                continue
            tbl[column]=tbl[column]*100

        # Final table
        tbl=tbl.to_dict('records')

        return [tbl]

    def updateUploadTestResultsStatus(self, interval, tab):
        # Parse new data
        panel=self.panels[int(tab)]

        # Terminal output
        post_msg = ''

        if len(panel.dbUploadString.contents) > 0:
            post_msg = 'Upload finished for '+str(len(panel.dbUploadString.contents)) + ' PBs ('+str(panel.dbUploadString.nContents)+' files).'
            if panel.dbUploadString.hasError:
                post_msg += ' Some uploads failed, see terminal output.'
            if panel.dbUploadString.hasWarning:
                post_msg += ' Some uploads have warning, see terminal output.'

        return post_msg

    def updateTransitionStatus(self, interval, tab):
        # Parse new data
        panel=self.panels[int(tab)]
        panel.parse()

        # Terminal output
        panel_str = ''.join(panel.data)

        #clear message once main tests started
        if "===>Transition panel" not in panel_str:
            return ''

        if '===>Transition finished for panel' in panel_str:
            return 'Transition finished!'
        if '===>ILLEGAL_STAGE' in panel_str:
            return 'Illegal stege! Check terminal output.'
        if '===>NO_TEST' in panel_str or '===>MISSING_TEST' in panel_str or '===>FAILED_TEST' in panel_str:
            return 'Missing/failed tests in current stage! Check terminal output.'

        return ''

    def updateVEndata(self, interval, tab):
        # Parse new data
        panel=self.panels[int(tab)]        
        if panel.refreshDisplayResult_VEn == False:
            raise PreventUpdate

        panel.refreshDisplayResult_VEn = False
        ven = pd.DataFrame(data={'pb':range(10)})

        # PADID
        panel_padi_data = panel.jsonparser.get_test_results("PADID", [("PADID", [-0.5, 0.5])], category=0)
        if not panel_padi_data.empty:
            ven=ven.merge(panel_padi_data, on='pb', how='outer')

        # BER        
        panel_bert_data = panel.jsonparser.get_test_results("BER", [("RELIABILITY", [0.999999, 1.000001])], category=0)
        if not panel_bert_data.empty:
            ven=ven.merge(panel_bert_data, on='pb', how='outer')

        # LV ENABLE 
        panel_lven_data = panel.jsonparser.get_test_results("LV_ENABLE", [("VOUT", [1.40, 1.60, -1.0, 0.1])], category=1, OnOff=[1,0])
        if not panel_lven_data.empty:
            ven=ven.merge(panel_lven_data, on='pb', how='outer')

        # HV ENABLE 
        cuts_hven = [
            ("AMACHVRET", [300., 2000., -1., 200.]), # on low, on high, off low, off high
            ("HVIIN", [0.8e-3, 2.0e-3, -1., 2.0e-3]),
            ("HVIOUT", [0.8e-3, 2.0e-3, -1., 2.0e-6])
        ]

        panel_hven_data = panel.jsonparser.get_test_results("HV_ENABLE", cuts_hven, category=1, OnOff=[1,0])
        if not panel_hven_data.empty:
            ven=ven.merge(panel_hven_data, on='pb', how='outer')

        # OF
        panel_ofin_data = panel.jsonparser.get_test_results("OF", [("linPOLV", [1.3, 2.0, -1.0, 0.25])], category=0, OnOff=[0,1])
        if not panel_ofin_data.empty:
            ven=ven.merge(panel_ofin_data, on='pb', how='outer')

        ven = ven.to_dict('records')

        return [ven]
    
    def updateTIOData(self, interval, tab):
        # Parse new data
        panel=self.panels[int(tab)]
        if panel.refreshDisplayResult_TIO == False:
            raise PreventUpdate

        panel.refreshDisplayResult_TIO = False
        # Toggle output tables
        tio=pd.DataFrame(data={'pb':range(10)})

        cuts_tou = [
            ("OFout_value", [1.0, 1.5, -0.01, 0.01]), # on low, on high, off low, off high
            ("CALx_value", [0.75, 1.0, -0.1, 0.1]),
            ("CALy_value", [0.75, 1.0, -0.1, 0.1]),
            ("LDx0EN_value", [1.0, 1.5, -0.01, 0.01]),
            ("LDx1EN_value", [1.0, 1.5, -0.01, 0.01]),
            ("LDx2EN_value", [1.0, 1.5, -0.01, 0.01]),
            ("LDy0EN_value", [1.0, 1.5, -0.01, 0.01]),
            ("LDy1EN_value", [1.0, 1.5, -0.01, 0.01]),
            ("LDy2EN_value", [1.0, 1.5, -0.01, 0.01]),
            ("Shuntx_value", [0.95, 1.2, 0.1, 0.3]),
            ("Shunty_value", [0.95, 1.2, 0.1, 0.3])
        ]
        panel_tou_data = panel.jsonparser.get_test_results("TOGGLEOUTPUT", cuts_tou, category=1, OnOff=[1,0])
        panel_tou_data.rename(columns=lambda x: x.replace('_value', ''), inplace=True)
        if not panel_tou_data.empty:
            tio=tio.merge(panel_tou_data, on='pb', how='outer')
       # 
        tio=tio.to_dict('records')

        return [tio]

    def updateLVIVdata(self, interval, tab):
        # Parse new data
        panel=self.panels[int(tab)]

        if panel.refreshDisplayResult_LVIVdata == False:
            raise PreventUpdate

        panel.refreshDisplayResult_LVIVdata = False

        # get dataframe from json

        panel_lviv_data = panel.jsonparser.get_test_results("VIN", ["VIN", "AMACDCDCIN"], category=2, get_array=True)
        if panel_lviv_data.empty:
            return [[]]

        panel_lviv_data.rename(columns=lambda x: x.replace('value_', ''), inplace=True)
        panel_lviv_data = pd.concat([dataframe_expand_rows(row, panel_lviv_data.columns, 'pb') for _, row in panel_lviv_data.iterrows()]).reset_index(drop=True)

        # Prepare data
        p_lviv=panel_lviv_data.pb.unique()
        d_lviv=[]
        for pb in range(0,10):
            d_lviv.append({
                'VIN'       : panel_lviv_data[panel_lviv_data.pb==pb].VIN        if pb in p_lviv else [],
                'AMACDCDCIN': panel_lviv_data[panel_lviv_data.pb==pb].AMACDCDCIN if pb in p_lviv else []
                })

        return [d_lviv]

    def updateAMSlopedata(self, interval, tab):
        # Parse new data
        panel=self.panels[int(tab)]

        if panel.refreshDisplayResult_AMSlope == False:
            raise PreventUpdate

        panel.refreshDisplayResult_AMSlope = False

        # get data from json
        panel_amsl_data = panel.jsonparser.get_test_results("AMSLOPE", ["CALIN", "AMACCAL"], category=2, get_array=True)
        if panel_amsl_data.empty:
            return [[]]

        panel_amsl_data.rename(columns=lambda x: x.replace('value_', ''), inplace=True)
        panel_amsl_data = pd.concat([dataframe_expand_rows(row, panel_amsl_data.columns, 'pb') for _, row in panel_amsl_data.iterrows()]).reset_index(drop=True)

        # Prepare data
        p_amsl=panel_amsl_data.pb.unique()
        d_amsl=[]
        for pb in range(0,10):
            d_amsl.append({
                'CALIN'  :panel_amsl_data[panel_amsl_data.pb==pb].CALIN   if pb in p_amsl else [],
                'AMACCAL':panel_amsl_data[panel_amsl_data.pb==pb].AMACCAL if pb in p_amsl else []
                })

        return [d_amsl]

    def updateDACdata(self, interval, tab):
        # Parse new data
        panel=self.panels[int(tab)]

        if panel.refreshDisplayResult_DACdata == False:
            raise PreventUpdate

        panel.refreshDisplayResult_DACdata = False

        d_ramp={'rshx':[], 'rshy':[], 'rcax':[], 'rcay':[]}
        # get data from json
        panel_rshx_data = get_dataframe_DAC(panel, "RAMPDACShuntx", ["DACShuntx", "value", "AMACSHUNTX"])
        panel_rshy_data = get_dataframe_DAC(panel, "RAMPDACShunty", ["DACShunty", "value", "AMACSHUNTY"])
        panel_rcax_data = get_dataframe_DAC(panel, "RAMPDACCalx", ["DACCalx", "value", "AMACCALX"])
        panel_rcay_data = get_dataframe_DAC(panel, "RAMPDACCaly", ["DACCaly", "value", "AMACCALY"])

        if panel_rshx_data.empty or panel_rshy_data.empty or panel_rcax_data.empty or panel_rcay_data.empty:
            return [d_ramp]

        #
        # Empty dataframe with data
        p_rshx=panel_rshx_data.pb.unique()
        p_rshy=panel_rshy_data.pb.unique()
        p_rcax=panel_rcax_data.pb.unique()
        p_rcay=panel_rcay_data.pb.unique()
        for pb in range(0,10):
            pbdata=panel_rshx_data[panel_rshx_data.pb==pb]
            d_ramp['rshx'].append({col:pbdata[col] if pb in p_rshx else [] for col in pbdata.columns})

            pbdata=panel_rshy_data[panel_rshy_data.pb==pb]
            d_ramp['rshy'].append({col:pbdata[col] if pb in p_rshy else [] for col in pbdata.columns})

            pbdata=panel_rcax_data[panel_rcax_data.pb==pb]
            d_ramp['rcax'].append({col:pbdata[col] if pb in p_rcax else [] for col in pbdata.columns})

            pbdata=panel_rcay_data[panel_rcay_data.pb==pb]
            d_ramp['rcay'].append({col:pbdata[col] if pb in p_rcay else [] for col in pbdata.columns})

        return [d_ramp]

    def updateDCDCdata(self, interval, tab):
        # Parse new data
        panel=self.panels[int(tab)]

        if panel.refreshDisplayResult_DCDCdata == False:
            raise PreventUpdate

        panel.refreshDisplayResult_DCDCdata = False

        # get dataframe from json

        panel_dcdc_data = panel.jsonparser.get_test_results("DCDCEFFICIENCY", ["IOUTSET", "IOUT", "EFFICIENCY", "AMACPTAT", "AMACCUR10V", "AMACCUR1V"], category=2, get_array=True)
        if panel_dcdc_data.empty:
            t_empty = pd.DataFrame(data={'pb':range(10)}).to_dict('records')
            return t_empty, []

        panel_dcdc_data.rename(columns=lambda x: x.replace('value_', ''), inplace=True)
        panel_dcdc_data = pd.concat([dataframe_expand_rows(row, panel_dcdc_data.columns, 'pb') for _, row in panel_dcdc_data.iterrows()]).reset_index(drop=True)

        # Graph data
        p_dcdc=panel_dcdc_data.pb.unique()
        d_dcdc=[]
        for pb in range(0,10):
            d_dcdc.append({
                'IOUT'      :panel_dcdc_data[panel_dcdc_data.pb==pb].IOUT       if pb in p_dcdc else [],
                'EFFICIENCY':panel_dcdc_data[panel_dcdc_data.pb==pb].EFFICIENCY if pb in p_dcdc else [],
                'AMACPTAT'  :panel_dcdc_data[panel_dcdc_data.pb==pb].AMACPTAT   if pb in p_dcdc else [],
                'AMACCUR10V':panel_dcdc_data[panel_dcdc_data.pb==pb].AMACCUR10V if pb in p_dcdc else [],
                'AMACCUR1V' :panel_dcdc_data[panel_dcdc_data.pb==pb].AMACCUR1V  if pb in p_dcdc else []
                })

        # DCDC efficiency at 2A
        dcdc2=panel_dcdc_data[abs(panel_dcdc_data.IOUTSET-2.0)<0.001].rename(columns={
            'EFFICIENCY':'value_EFF2A',
            'AMACPTAT':'value_AMACPTAT2A',
        })
        dcdc2['error_EFF2A']=dcdc2.value_EFF2A<0.6

        dcdc2=pad_dataframe_pbs(dcdc2)
        t_dcdc=dcdc2.to_dict('records')
       
        return t_dcdc, d_dcdc

    def updateHVdata(self, interval, tab):
        # Parse new data
        panel=self.panels[int(tab)]

        if panel.refreshDisplayResult_HVdata == False:
            raise PreventUpdate

        panel.refreshDisplayResult_HVdata = False

        # get dataframe from json

        panel_hvrt_data = panel.jsonparser.get_test_results("HVSENSE", ["HVIOUT", "AMACGAIN0", "AMACGAIN1", "AMACGAIN2", "AMACGAIN4", "AMACGAIN8"], category=2, get_array=True)
        if panel_hvrt_data.empty:
            return [[]]

        panel_hvrt_data.rename(columns=lambda x: x.replace('value_', ''), inplace=True)
        panel_hvrt_data = pd.concat([dataframe_expand_rows(row, panel_hvrt_data.columns, 'pb') for _, row in panel_hvrt_data.iterrows()]).reset_index(drop=True)

        #
        # Graphs
        p_gains=panel_hvrt_data.pb.unique()
        p_hvrt=panel_hvrt_data.pb.unique()
        d_hvrt=[]
        for pb in range(0,10):
            d_hvrt.append({
                'HVIOUT'   :panel_hvrt_data[panel_hvrt_data.pb==pb].HVIOUT    if pb in p_hvrt else [],
                'AMACGAIN0':panel_hvrt_data[panel_hvrt_data.pb==pb].AMACGAIN0 if pb in p_hvrt else [],
                'AMACGAIN1':panel_hvrt_data[panel_hvrt_data.pb==pb].AMACGAIN1 if pb in p_hvrt else [],
                'AMACGAIN2':panel_hvrt_data[panel_hvrt_data.pb==pb].AMACGAIN2 if pb in p_hvrt else [],
                'AMACGAIN4':panel_hvrt_data[panel_hvrt_data.pb==pb].AMACGAIN4 if pb in p_hvrt else [],
                'AMACGAIN8':panel_hvrt_data[panel_hvrt_data.pb==pb].AMACGAIN8 if pb in p_hvrt else []
                })

        return [d_hvrt]


