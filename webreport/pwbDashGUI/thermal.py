import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State, ClientsideFunction
from dash.exceptions import PreventUpdate

import codecs
import pickle
import datetime
import pkg_resources
import sys
from time import time, sleep
from datetime import timedelta

import os

import plotly.express as px
import pandas as pd

from webreport.pwbDashCommon import template

from database import pwbdb
from database import pwbdbtools
from database import pwbdbpandas
from database import transitions
from database.pbv3_panel_transition import panel_transition

import itkdb.core
from . import chillerControler

def parseContent(inString, keyword):
    key1 = keyword + ':---> '
    key2 = ' <---:'+keyword
    idx_begin = inString.rfind(key1)
    idx_end = inString.rfind(key2)
    if idx_begin == -1 or idx_end == -1:
        return ""
    return inString[idx_begin+len(key1):idx_end]

def get_panel_stage_tests(client, panelid):
    stage = ""
    test_str = ""

    try:
        panel = transitions.Panel(panelid, client=client)
        stage = panel.currentStage
        for pwb in panel.children['PWB'].values():
            r = client.get('listTestRunsByComponent',json={'component':pwb.code, "stage":stage, "testType":"TEMPERATURE"})
            testRuns=list(filter(lambda t: t['state']!='deleted', r))
            testRuns_id = []
            for testRun in testRuns:
                testRuns_id.append(testRun['id'])
            if len(testRuns_id) == 0:
                continue
            testResults = client.get('getTestRunBulk', json={'testRun':testRuns_id})
            for testResult in testResults:
                NTCpbSenseRange = next((item["value"] for item in testResult["results"] if item["code"] == "NTCpbSenseRange"), -1)
                if NTCpbSenseRange == 0:
                    test_str = "cold"
                    break
                else:
                    test_str = "warm"
            if test_str ==  "cold":
                break
    except Exception:
        print("can not get stage and tests for panel "+panelid)

    print("Panel "+panelid+" stage = "+stage+", test = "+test_str)
    return [stage, test_str]


def string_to_hex(input_string):
    return ''.join(format(ord(char)+1, '02x') for char in input_string)

class ThermalPage:
    def __init__(self, cfg, app, port=5000):
        # Create the layout
        self.cfg=cfg

        self.chillers=[]
        self.chillers_isON=[]
        self.chillers_measTemp=[]
        self.chillers_targetTemp=[]
        self.chillers_cycle_done = []

        self.df_temp = pd.DataFrame({"time":[], "temp":[], "tab": []})

        chillerlabels=[]
        for chillercfg in cfg.chillers:
            chillercfg=chillercfg.copy()
            name  =chillercfg.pop('name')
            runner='LabRemoteRunner'

            chillerobj=getattr(chillerControler,runner)(**chillercfg)

            self.chillers.append(chillerobj)
            self.chillers_isON.append(None)
            self.chillers_measTemp.append(None)
            self.chillers_targetTemp.append(None)
            self.chillers_cycle_done.append(False)

            chillerlabels.append(name)

        self.command_dir = ""
        self.testing_ports = [] # up_0To4, up_5To9, down_0To4, down_5To9
        self.autoQC_panels = []
        self.autoQC_versions = []
        self.autoQC_batches = []
        self.autoQC_boards = []
        self.autoQC_serials = []

        self.autoQC_status = 0 # status code
        self.autoQC_status_copy = -1 # status code
        self.autoQC_start = time()
        self.autoQC_end = time()
        self.port=port

        if cfg.autoQC_cfg is not None:
            self.command_dir = cfg.autoQC_cfg['command_dir']
            self.testing_ports.append(cfg.autoQC_cfg.pop('port_up_0To4'))
            self.testing_ports.append(cfg.autoQC_cfg.pop('port_up_5To9'))
            self.testing_ports.append(cfg.autoQC_cfg.pop('port_down_0To4'))
            self.testing_ports.append(cfg.autoQC_cfg.pop('port_down_5To9'))

            # create command_dir if not exist
            os.makedirs(self.command_dir, exist_ok=True)

        self.stages_dict = {
            "BONDED": ["", "warm", "cold"],
            "THERMAL": ["", "warm"],
            "BURN_IN": ["", "warm", "cold"]
        }

        # Layout related stuff
        runradios=[
            dcc.RadioItems(id='debugThermal', options = [
                {'label': 'None'       , 'value': 0},
                {'label': 'Debug'      , 'value': 1},
                {'label': 'Super Debug', 'value': 4}
                ], value=0)
            ]
        rundropdowns=[
            dcc.Dropdown(id='transition-dropdown-group', options = [
            {'label': 'SMD Loading', 'value': 'SMD_LOAD'},
            {'label': 'Coil and Shield Loading', 'value': 'MAN_LOAD'},
            {'label': 'Die Attachment and Bonding', 'value': 'BONDED'},
            {'label': 'Thermal Cycling', 'value': 'THERMAL'},
            {'label': 'Burn-In', 'value': 'BURN_IN'},
            {'label': 'Module Reception',   'value': 'MODULE_RCP'},
            {'label': 'Loaded on a Module', 'value': 'LOADED'},
            {'label': 'Loaded on a Hybrid Burn-in Carrier', 'value': 'HYBBURN'}
            ], value='MODULE_RCP')
            ]
        runMonitorChecks=[
            dcc.Checklist(id='save-chiller-csv',    options = [{'label' : 'Save to CSV', 'value' : 1}], value = [1]),
            dcc.Checklist(id='save-chiller-influx', options = [{'label' : 'Save to InfluxDB', 'value' : 1}], value = [1])
            ]

        self.layout = template.jinja2_to_dash(
            pkg_resources.resource_filename(__name__,'template'),
            'thermal.html', replace=runradios+rundropdowns+runMonitorChecks, chillers=chillerlabels)

        #
        # Callbacks
        app.callback(
            [
                Output('display-test-chiller', 'children'),
            ],
            [Input('tabsThermal','value'), Input('interval-component-run-chiller', 'n_intervals')]
        )(self.updateRunData)

        app.callback(
            Output('chiller-status', 'data'),
            [Input('interval-component-status-chiller', 'n_intervals')]
        )(self.updateRunStatus)


        app.callback(
            Output('temp-monitor-graph', 'figure'),
            [Input('interval-component-run-chiller', 'n_intervals'), Input('tabsThermal','value')],
        )(self.updateTempGraph)


        app.callback(
            Output('turn_on_message', 'children'),
            [Input('turn_on_button','n_clicks')],
            [
                State('tabsThermal', 'value'),
                State('debugThermal', 'value'),
                State('save-chiller-csv', 'value'),
                State('save-chiller-influx', 'value')
            ],
            prevent_initial_call=True)(self.runTurnOn)
 
        app.callback(
            Output('turn_off_message', 'children'),
            [Input('turn_off_button','n_clicks')],
            [
                State('tabsThermal', 'value'),
                State('debugThermal', 'value'),
                State('save-chiller-csv', 'value'),
                State('save-chiller-influx', 'value')
            ],
            prevent_initial_call=True)(self.runTurnOff)

        app.callback(
            Output('chiller_status', 'children'),
            [Input('interval-component-run-chiller', 'n_intervals'), Input('tabsThermal','value')],
        )(self.updateChillerStatus)


        app.callback(
            Output('set_temp_message', 'children'),
            [Input('set_temp_button','n_clicks')],
            [
                State('set_temp', 'value'),
                State('tabsThermal', 'value'),
                State('debugThermal', 'value'),
                State('save-chiller-csv', 'value'),
                State('save-chiller-influx', 'value')
            ],
            prevent_initial_call=True)(self.runSetTemp)

        app.callback(
            Output('get_temp_message', 'children'),
            [Input('get_temp_button','n_clicks')],
            [
                State('tabsThermal', 'value'),
                State('debugThermal', 'value'),
                State('save-chiller-csv', 'value'),
                State('save-chiller-influx', 'value')
            ],
            prevent_initial_call=True)(self.runGetTemp)

        app.callback(
            Output('get_temp', 'children'),
            [Input('interval-component-run-chiller', 'n_intervals'), Input('tabsThermal','value')],
        )(self.updateGetTempData)

        app.callback(
            Output('meas_temp_message', 'children'),
            [Input('meas_temp_button','n_clicks')],
            [
                State('tabsThermal', 'value'),
                State('debugThermal', 'value'),
                State('save-chiller-csv', 'value'),
                State('save-chiller-influx', 'value')
            ],
            prevent_initial_call=True)(self.runMeasTemp)

        app.callback(
            Output('meas_temp', 'children'),
            [Input('interval-component-run-chiller', 'n_intervals'), Input('tabsThermal','value')],
        )(self.updateMeasTempData)

        app.callback(
            Output('set_ramp_message', 'children'),
            [Input('set_ramp_button','n_clicks')],
            [
                State('set_ramp', 'value'),
                State('tabsThermal', 'value'),
                State('debugThermal', 'value'),
                State('save-chiller-csv', 'value'),
                State('save-chiller-influx', 'value')
            ],
            prevent_initial_call=True)(self.runSetRamp)
  
        app.callback(
            Output('get_ramp_message', 'children'),
            [Input('get_ramp_button','n_clicks')],
            [
                State('tabsThermal', 'value'),
                State('debugThermal', 'value'),
                State('save-chiller-csv', 'value'),
                State('save-chiller-influx', 'value')
            ],
            prevent_initial_call=True)(self.runGetRamp)

        app.callback(
            Output('get_ramp', 'children'),
            [Input('interval-component-run-chiller', 'n_intervals'), Input('tabsThermal','value')],
        )(self.updateGetRampData)

        app.callback(
            Output('start_monitor_message', 'children'),
            [Input('start_monitor_button','n_clicks')],
            [
                State('tabsThermal', 'value'),
                State('debugThermal', 'value'),
                State('save-chiller-csv', 'value'),
                State('save-chiller-influx', 'value')
            ],
            prevent_initial_call=True)(self.runStartMonitor)

        app.callback(
            Output('stop_monitor_message', 'children'),
            [Input('stop_monitor_button','n_clicks')],
            [
                State('tabsThermal', 'value'),
                State('debugThermal', 'value'),
            ],
            prevent_initial_call=True)(self.runStopMonitor)
 
        app.callback(
            Output('clear_monitor_data_message', 'children'),
            [Input('clear_monitor_data_button','n_clicks')],
            [
                State('tabsThermal', 'value'),
                State('debugThermal', 'value'),
            ],
            prevent_initial_call=True)(self.runClearMonitor)

        app.callback(
            Output('thermal_cycle_message', 'children'),
            [Input('thermal_cycle_button','n_clicks')],
            [
                State('low_temp', 'value'),
                State('high_temp', 'value'),
                State('downRR', 'value'),
                State('upRR', 'value'),
                State('stay_low', 'value'),
                State('stay_high', 'value'),
                State('nCycle', 'value'),
                State('endTemp', 'value'),
                State('tabsThermal', 'value'),
                State('debugThermal', 'value'),
                State('save-chiller-csv', 'value'),
                State('save-chiller-influx', 'value')
            ],
            prevent_initial_call=True)(self.runStartThermalCycle)

        app.callback(
            Output('thermal_cycle_status_message', 'children'),
            [Input('interval-component-run-chiller', 'n_intervals'), Input('tabsThermal','value')],
        )(self.updateThermalCycleStatus)


        app.callback(
            Output('thermal_cycle_stop_message', 'children'),
            [Input('thermal_cycle_stop_button','n_clicks')],
            [
                State('tabsThermal', 'value'),
                State('debugThermal', 'value'),
            ],
            prevent_initial_call=True)(self.runStopThermalCycle)

        app.callback(
            Output('start_auto_qc_message', 'children'),
            [Input('start_auto_qc_button','n_clicks')],
            [
                State('tabsThermal', 'value'),
                State('debugThermal', 'value'),
                State('save-chiller-csv', 'value'),
                State('save-chiller-influx', 'value'),
                State('itkdb_auth', 'data'),
                State('itkdb_auth_pass1', 'data'),
                State('itkdb_auth_pass2', 'data'),
                State('low_temp', 'value'),
                State('high_temp', 'value'),
                State('downRR', 'value'),
                State('upRR', 'value'),
                State('stay_low', 'value'),
                State('stay_high', 'value'),
                State('nCycle', 'value'),
                State('endTemp', 'value'),
                State('burn_in_current_load', 'value'),
                State('burn_in_time', 'value'),
                State('panel_up_p0', 'value'),
                State('panel_up_p1', 'value'),
                State('panel_up_p2', 'value'),
                State('panel_up_p3', 'value'),
                State('panel_up_p4', 'value'),
                State('panel_up_p5', 'value'),
                State('panel_up_p6', 'value'),
                State('panel_up_p7', 'value'),
                State('panel_up_p8', 'value'),
                State('panel_up_p9', 'value'),
                State('panel_down_p0', 'value'),
                State('panel_down_p1', 'value'),
                State('panel_down_p2', 'value'),
                State('panel_down_p3', 'value'),
                State('panel_down_p4', 'value'),
                State('panel_down_p5', 'value'),
                State('panel_down_p6', 'value'),
                State('panel_down_p7', 'value'),
                State('panel_down_p8', 'value'),
                State('panel_down_p9', 'value')
            ],
            prevent_initial_call=True)(self.runStartAutoQC)

        app.callback(
            Output('run_basic_test_message', 'children'),
            [Input('run_basic_test_button','n_clicks')],
            [
                State('tabsThermal', 'value'),
                State('debugThermal', 'value'),
                State('save-chiller-csv', 'value'),
                State('save-chiller-influx', 'value'),
                State('itkdb_auth', 'data'),
                State('itkdb_auth_pass1', 'data'),
                State('itkdb_auth_pass2', 'data'),
                State('endTemp', 'value'),
                State('panel_up_p0', 'value'),
                State('panel_up_p1', 'value'),
                State('panel_up_p2', 'value'),
                State('panel_up_p3', 'value'),
                State('panel_up_p4', 'value'),
                State('panel_up_p5', 'value'),
                State('panel_up_p6', 'value'),
                State('panel_up_p7', 'value'),
                State('panel_up_p8', 'value'),
                State('panel_up_p9', 'value'),
                State('panel_down_p0', 'value'),
                State('panel_down_p1', 'value'),
                State('panel_down_p2', 'value'),
                State('panel_down_p3', 'value'),
                State('panel_down_p4', 'value'),
                State('panel_down_p5', 'value'),
                State('panel_down_p6', 'value'),
                State('panel_down_p7', 'value'),
                State('panel_down_p8', 'value'),
                State('panel_down_p9', 'value')
            ],
            prevent_initial_call=True)(self.runBasicTests)

        app.callback(
            Output('run_full_test_message', 'children'),
            [Input('run_full_test_button','n_clicks')],
            [
                State('tabsThermal', 'value'),
                State('debugThermal', 'value'),
                State('save-chiller-csv', 'value'),
                State('save-chiller-influx', 'value'),
                State('itkdb_auth', 'data'),
                State('itkdb_auth_pass1', 'data'),
                State('itkdb_auth_pass2', 'data'),
                State('endTemp', 'value'),
                State('panel_up_p0', 'value'),
                State('panel_up_p1', 'value'),
                State('panel_up_p2', 'value'),
                State('panel_up_p3', 'value'),
                State('panel_up_p4', 'value'),
                State('panel_up_p5', 'value'),
                State('panel_up_p6', 'value'),
                State('panel_up_p7', 'value'),
                State('panel_up_p8', 'value'),
                State('panel_up_p9', 'value'),
                State('panel_down_p0', 'value'),
                State('panel_down_p1', 'value'),
                State('panel_down_p2', 'value'),
                State('panel_down_p3', 'value'),
                State('panel_down_p4', 'value'),
                State('panel_down_p5', 'value'),
                State('panel_down_p6', 'value'),
                State('panel_down_p7', 'value'),
                State('panel_down_p8', 'value'),
                State('panel_down_p9', 'value')
            ],
            prevent_initial_call=True)(self.runFullTests)

        app.callback(
            Output('run_stage_transition_message', 'children'),
            [Input('run_stage_transition_button','n_clicks')],
            [
                State('transition-dropdown-group', 'value'),
                State('tabsThermal', 'value'),
                State('debugThermal', 'value'),
                State('save-chiller-csv', 'value'),
                State('save-chiller-influx', 'value'),
                State('itkdb_auth', 'data'),
                State('itkdb_auth_pass1', 'data'),
                State('itkdb_auth_pass2', 'data'),
                State('endTemp', 'value'),
                State('panel_up_p0', 'value'),
                State('panel_up_p1', 'value'),
                State('panel_up_p2', 'value'),
                State('panel_up_p3', 'value'),
                State('panel_up_p4', 'value'),
                State('panel_up_p5', 'value'),
                State('panel_up_p6', 'value'),
                State('panel_up_p7', 'value'),
                State('panel_up_p8', 'value'),
                State('panel_up_p9', 'value'),
                State('panel_down_p0', 'value'),
                State('panel_down_p1', 'value'),
                State('panel_down_p2', 'value'),
                State('panel_down_p3', 'value'),
                State('panel_down_p4', 'value'),
                State('panel_down_p5', 'value'),
                State('panel_down_p6', 'value'),
                State('panel_down_p7', 'value'),
                State('panel_down_p8', 'value'),
                State('panel_down_p9', 'value')
            ],
            prevent_initial_call=True)(self.runStageTransition)


        app.callback(
            Output('stop_auto_qc_message', 'children'),
            [Input('stop_auto_qc_button','n_clicks')],
            [
                State('tabsThermal', 'value'),
                State('debugThermal', 'value')
            ],
            prevent_initial_call=True)(self.runStopAutoQC)

        app.callback(
            Output('auto_qc_status_message', 'children'),
            [Input('interval-component-run-chiller', 'n_intervals'), Input('tabsThermal','value')],
        )(self.updateAutoQCStatus)



        ## Client-side Callbacks

        # Store checkbox settings per chiller
        chillerstoreelements=['debugThermal']

        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='storeChillerInfo'
                ),
            Output('chiller-storage', 'data'),
            [Input(name, 'value') for name in chillerstoreelements],
            [State('chiller-storage', 'data'), State('tabsThermal','value')])

        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='restoreChillerInfo'
                ),
            [Output(name, 'value') for name in chillerstoreelements],
            [Input('tabsThermal', 'value')],
            [State('chiller-storage', 'data')])

        # Update status styles
        app.clientside_callback(
            ClientsideFunction(
                namespace='clientside',
                function_name='updateTabColors'
                ),
            [o for p in range(len(self.chillers)) for o in [Output(f'tabThermal-{p}', 'style'),Output(f'tabThermal-{p}', 'selected_style')]],
            [Input('chiller-status', 'data')],
            prevent_initial_call=True
            )

    def updateRunData(self, tab, interval):
        # Parse new data
        start=time()
        chiller=self.chillers[int(tab)]
        chiller.parse()

        # Terminal output
        terminal = '\n'.join(chiller.data)

        return [terminal]

    def updateRunStatus(self, interval):
        return [chiller.running for chiller in self.chillers]

    def updateTempGraph(self, interval, tab):
        chiller=self.chillers[int(tab)]
        chiller.parse()
        get_msg = ''
        get_msg = ''.join(chiller.data)
        time=parseContent(get_msg, 'MONITOR_TIME_SECONDS')
        temp=parseContent(get_msg, 'MeasuredTemperature')
        if time == "" or temp == "":
            raise PreventUpdate
        time_s = datetime.datetime.fromtimestamp(int(time))
        if len(self.df_temp.index) > 0:
            if time_s == self.df_temp.iloc[-1]['time']:
                raise PreventUpdate
        self.df_temp.loc[len(self.df_temp.index)] = [time_s, float(temp), int(tab)]

        fig = px.line(self.df_temp[self.df_temp['tab'] == int(tab)], x='time', y='temp', width=1000, height=500)
        fig.update_layout(transition_duration=1000, xaxis_title="time", yaxis_title="temperature [C]")

        return fig

    def runTurnOn_execute(self, tab, debug, save_csv, save_influx):
        try:
            save_csv_ = False
            save_influx_ = False
            if len(save_csv) > 0:
                save_csv_ = True
            if len(save_influx) > 0:
                save_influx_ = True

            self.chillers[int(tab)].run(option=['turn-on'], save_influx=save_influx_, save_csv=save_csv_, debug=debug)
        except:
            return sys.exc_info()[0]
        return ''


    def runTurnOn(self, button, tab, debug, save_csv, save_influx):
        if button==None:
            raise PreventUpdate

        return self.runTurnOn_execute(tab, debug, save_csv, save_influx)

    def runTurnOff_execute(self, tab, debug, save_csv, save_influx):
        try:
            save_csv_ = False
            save_influx_ = False
            if len(save_csv) > 0:
                save_csv_ = True
            if len(save_influx) > 0:
                save_influx_ = True

            self.chillers[int(tab)].run(option=['turn-off'], save_influx=save_influx_, save_csv=save_csv_, debug=debug)
        except:
            return sys.exc_info()[0]
        return 'Wait 1 min before turn back ON!'

    def runTurnOff(self, button, tab, debug, save_csv, save_influx):
        if button==None:
            raise PreventUpdate

        return self.runTurnOff_execute(tab, debug, save_csv, save_influx)

    def updateChillerStatus(self, interval, tab):
        chiller=self.chillers[int(tab)]
        chiller.parse()
        get_msg = ''
        get_msg = ''.join(chiller.data)
        content=parseContent(get_msg, 'ChillerStatus')
        if content == "":
            raise PreventUpdate
        if content == "ON":
            self.chillers_isON[int(tab)] = True
        if content == "OFF":
            self.chillers_isON[int(tab)] = False

        return "Chiller is "+content

    def runSetTemp_execute(self, set_temp, tab, debug, save_csv, save_influx):
        if set_temp == '':
            return 'Please specify value:'
        try:
            save_csv_ = False
            save_influx_ = False
            if len(save_csv) > 0:
                save_csv_ = True
            if len(save_influx) > 0:
                save_influx_ = True

            self.chillers[int(tab)].run(option=['set-temp', '--', str(set_temp)], save_influx=save_influx_, save_csv=save_csv_, debug=debug)
        except:
            return sys.exc_info()[0]
        return ''

    def runSetTemp(self, button, set_temp, tab, debug, save_csv, save_influx):
        if button==None:
            raise PreventUpdate

        return self.runSetTemp_execute(set_temp, tab, debug, save_csv, save_influx)

    def runGetTemp(self, button, tab, debug, save_csv, save_influx):
        if button==None:
            raise PreventUpdate

        try:
            save_csv_ = False
            save_influx_ = False
            if len(save_csv) > 0:
                save_csv_ = True
            if len(save_influx) > 0:
                save_influx_ = True

            self.chillers[int(tab)].run(option=['get-temp'], save_influx=save_influx_, save_csv=save_csv_, debug=debug)
        except:
            return sys.exc_info()[0]
        return ''

    def updateGetTempData(self, interval, tab):
        chiller=self.chillers[int(tab)]
        chiller.parse()
        get_msg = ''
        get_msg = ''.join(chiller.data)
        content=parseContent(get_msg, 'TargetTemperature')
        if content == "":
            raise PreventUpdate
        self.chillers_targetTemp[int(tab)] = float(content)

        return content

    def runMeasTemp(self, button, tab, debug, save_csv, save_influx):
        if button==None:
            raise PreventUpdate

        try:
            save_csv_ = False
            save_influx_ = False
            if len(save_csv) > 0:
                save_csv_ = True
            if len(save_influx) > 0:
                save_influx_ = True

            self.chillers[int(tab)].run(option=['meas-temp'], save_influx=save_influx_, save_csv=save_csv_, debug=debug)
        except:
            return sys.exc_info()[0]
        return ''

    def updateMeasTempData(self, interval, tab):
        chiller=self.chillers[int(tab)]
        chiller.parse()
        get_msg = ''
        get_msg = ''.join(chiller.data)
        content=parseContent(get_msg, 'MeasuredTemperature')
        if content == "":
            raise PreventUpdate
        self.chillers_measTemp[int(tab)] = float(content)

        return content

    def runSetRamp(self, button, set_ramp, tab, debug, save_csv, save_influx):
        if button==None:
            raise PreventUpdate

        if set_ramp == '':
            return 'Please specify value:'
        v_set_ramp = float(set_ramp)
        if v_set_ramp < 0.1 or v_set_ramp > 5.5:
            return 'ramp should be within [0.1, 5.5]'
        try:
            save_csv_ = False
            save_influx_ = False
            if len(save_csv) > 0:
                save_csv_ = True
            if len(save_influx) > 0:
                save_influx_ = True

            self.chillers[int(tab)].run(option=['set-ramp', '--', str(set_ramp)], save_influx=save_influx_, save_csv=save_csv_, debug=debug)
        except:
            return sys.exc_info()[0]
        return ''

    def runGetRamp(self, button, tab, debug, save_csv, save_influx):
        if button==None:
            raise PreventUpdate

        try:
            save_csv_ = False
            save_influx_ = False
            if len(save_csv) > 0:
                save_csv_ = True
            if len(save_influx) > 0:
                save_influx_ = True

            self.chillers[int(tab)].run(option=['get-ramp'], save_influx=save_influx_, save_csv=save_csv_, debug=debug)
        except:
            return sys.exc_info()[0]
        return ''

    def updateGetRampData(self, interval, tab):
        chiller=self.chillers[int(tab)]
        chiller.parse()
        get_msg = ''
        get_msg = ''.join(chiller.data)
        content=parseContent(get_msg, 'RampRate')
        if content == "":
            raise PreventUpdate
        return content

    def runStartMonitor_execute(self, tab, debug, save_csv, save_influx):
        try:
            save_csv_ = False
            save_influx_ = False
            if len(save_csv) > 0:
                save_csv_ = True
            if len(save_influx) > 0:
                save_influx_ = True
            self.chillers[int(tab)].run(option=['meas-temp'], save_influx=save_influx_, save_csv=save_csv_, debug=debug)
        except:
            return sys.exc_info()[0]
        return ''

    def runStartMonitor(self, button, tab, debug, save_csv, save_influx):
        if button==None:
            raise PreventUpdate
        return self.runStartMonitor_execute(tab, debug, save_csv, save_influx)

    def runStopMonitor_execute(self, tab, debug):
        if not self.chillers[int(tab)].running:
            return 'Monitoring not running!'

        self.chillers[int(tab)].stop()

        return ''

    def runStopMonitor(self, button, tab, debug):
        if button==None:
            raise PreventUpdate

        
        return self.runStopMonitor_execute(tab, debug)

    def runClearMonitor_execute(self, tab, debug):

        self.df_temp = self.df_temp.iloc[0:0]

        return ''

    def runClearMonitor(self, button, tab, debug):
        if button==None:
            raise PreventUpdate

        return self.runClearMonitor_execute(tab, debug)

    def runStartThermalCycle_execute(self, low_temp, high_temp, downRR, upRR, stay_low, stay_high, nCycle, endTemp, tab, debug, save_csv, save_influx):
        if low_temp == '' or high_temp == '' or downRR == '' or upRR == '' or stay_low == '' or stay_high == '' or nCycle == '' or endTemp == '':
            return 'Incomplete input!!!'
        v_downRR = float(downRR)
        v_upRR = float(upRR)
        if v_downRR < 0.1 or v_downRR > 5.5 or v_upRR < 0.1 or v_upRR > 5.5:
            return 'ramp should be within [0.1, 5.5]'
        try:
            save_csv_ = False
            save_influx_ = False
            if len(save_csv) > 0:
                save_csv_ = True
            if len(save_influx) > 0:
                save_influx_ = True

            self.chillers[int(tab)].run(option=['thermal-cycle', '--', str(low_temp), str(high_temp), str(downRR), str(upRR), str(stay_low), str(stay_high), str(nCycle), str(endTemp)], save_influx=save_influx_, save_csv=save_csv_, debug=debug)
        except:
            return sys.exc_info()[0]
        return ''

    def runStartThermalCycle(self, button, low_temp, high_temp, downRR, upRR, stay_low, stay_high, nCycle, endTemp, tab, debug, save_csv, save_influx):
        if button==None:
            raise PreventUpdate

        return self.runStartThermalCycle_execute(low_temp, high_temp, downRR, upRR, stay_low, stay_high, nCycle, endTemp, tab, debug, save_csv, save_influx)

    def updateThermalCycleStatus(self, interval, tab):
        chiller=self.chillers[int(tab)]
        chiller.parse()
        get_msg = ''
        get_msg = ''.join(chiller.data)
        content=parseContent(get_msg, 'ThermalCycleStatus')
        if content == "":
            raise PreventUpdate
        if "thermal cycle finished" in content:
            self.chillers_cycle_done[int(tab)] = True

        return content

    def runStopThermalCycle_execute(self, tab, debug):
        if not self.chillers[int(tab)].running:
            return 'Thermal Cycle not running!'

        self.chillers[int(tab)].stop()

        return ''

    def runStopThermalCycle(self, button, tab, debug):
        if button==None:
            raise PreventUpdate

        return self.runStopThermalCycle_execute(tab, debug)

    def runOneQCCommand_execute(self, tab, debug, save_csv, save_influx, user, pass1, pass2, command, *args):

        self.autoQC_status = 0
        self.autoQC_start = time()
        if len(args) < 2:
            self.autoQC_status = 0
            return 'Error! Incomplete inputs!!!'

        return_msg = ""

        endTemp = float(args[0])

        self.autoQC_panels = []

        for idx in range(1, len(args)):
            self.autoQC_panels.append(args[idx])

        nEmptyPanels = len([s for s in self.autoQC_panels if s == ""])
        if nEmptyPanels == len(self.autoQC_panels):
            return_msg += ' The rack is empty. Please provide at least one panel number!!!'
        for idx in range(1):
            if args[idx] == '':
                return_msg += ' Please provide testing temperature!! (endTemp)'

        print("panels: ")
        print(self.autoQC_panels)

        # check ITk DB login
        if user==None:
            return_msg += ' Please login to ITk DB first!!!'
            return return_msg

        user=pickle.loads(codecs.decode(user.encode(),'base64'))

        if (not user.is_authenticated()) or (pass1 is None) or (pass2 is None):
            return_msg += ' Please login to ITk DB first!!!'    
            return return_msg

        if user.is_expired() or user.expires_in < 300:
            print('IDk DB login is expired or is expiring, re-login')
            user = itkdb.core.User(pass1, pass2)
            user.authenticate()
        
        if not user.is_authenticated():
            return_msg += ' Please login to ITk DB first!!!'
            return return_msg
        
        client=itkdb.Client(user=user)
        self.fillAutoQCPanels(pass1, pass2)

        if return_msg != "":
            return return_msg

        if self.command_dir == "" or len(self.testing_ports) == 0:
            return_msg += ' Please set the autoQC_cfg in the config file!'
            return return_msg

        ## clear chiller temp monitor graph

        self.runClearMonitor_execute(tab, debug)

        if "Tests" in command:
            self.autoQC_status = -1
            print("==== first, get stable chiller temp to "+str(endTemp))
            result_target_temp = self.get_stable_temp(endTemp, tab, debug, save_csv, save_influx)
            if result_target_temp != "":
                self.autoQC_status == 0
                return result_target_temp

            self.sendCommand(command, pass1, pass2)
            if self.autoQC_status == 0:
                return "AutoQC is stopped!"

            self.autoQC_end = time()
            self.autoQC_status = -2
            return_msg = command.split(",")[0]+" Finished!"

        if "panelStageTransition" in command:
            self.sendCommand(command, pass1, pass2)
            return_msg = "Panel Transition Finished!"

        return return_msg

    def runBasicTests(self, button, tab, debug, save_csv, save_influx, user, pass1, pass2, *args):
        if button==None:
            raise PreventUpdate
        return self.runOneQCCommand_execute(tab, debug, save_csv, save_influx, user, pass1, pass2, "BasicTests,BasicTests", *args)

    def runFullTests(self, button, tab, debug, save_csv, save_influx, user, pass1, pass2, *args):
        if button==None:
            raise PreventUpdate
        return self.runOneQCCommand_execute(tab, debug, save_csv, save_influx, user, pass1, pass2, "AllTests,AllTests", *args)

    def runStageTransition(self, button, stage, tab, debug, save_csv, save_influx, user, pass1, pass2, *args):
        if button==None:
            raise PreventUpdate
        if stage == None:
            return ' Please select a stage to transition to!'
        return self.runOneQCCommand_execute(tab, debug, save_csv, save_influx, user, pass1, pass2, "panelStageTransition,"+str(stage), *args)


    def runStartAutoQC(self, button, tab, debug, save_csv, save_influx, user, pass1, pass2, *args):
        if button==None:
            raise PreventUpdate

        self.autoQC_status = 0
        self.autoQC_start = time()
        if len(args) < 11:
            self.autoQC_status = 0
            return 'Error! Incomplete inputs!!!'

        return_msg = ""

        low_temp = args[0]
        high_temp = args[1]
        downRR = args[2]
        upRR = args[3]
        stay_low = args[4]
        stay_high = args[5]
        nCycle = args[6]
        endTemp = args[7]
        burn_in_current_load = args[8]
        burn_in_time = args[9]

        self.autoQC_panels = []

        for idx in range(10, len(args)):
            self.autoQC_panels.append(args[idx])

        nEmptyPanels = len([s for s in self.autoQC_panels if s == ""])
        if nEmptyPanels == len(self.autoQC_panels):
            return_msg += ' The rack is empty. Please provide at least one panel number!!!'
        for idx in range(10):
            if args[idx] == '':
                return_msg += ' Incomplete input for thermal cycle and burn-in setting!!!'
        v_downRR = float(downRR)
        v_upRR = float(upRR)
        if v_downRR < 0.1 or v_downRR > 5.5 or v_upRR < 0.1 or v_upRR > 5.5:
            return_msg += ' Ramp should be within [0.1, 5.5]!!!'

        print("panels: ")
        print(self.autoQC_panels)

        # check ITk DB login
        if user==None:
            return_msg += ' Please login to ITk DB first!!!'
            return return_msg

        user=pickle.loads(codecs.decode(user.encode(),'base64'))

        if (not user.is_authenticated()) or (pass1 is None) or (pass2 is None):
            return_msg += ' Please login to ITk DB first!!!'    
            return return_msg

        if user.is_expired() or user.expires_in < 300:
            print('IDk DB login is expired or is expiring, re-login')
            user = itkdb.core.User(pass1, pass2)
            user.authenticate()
        
        if not user.is_authenticated():
            return_msg += ' Please login to ITk DB first!!!'
            return return_msg
        
        client=itkdb.Client(user=user)
        self.fillAutoQCPanels(pass1, pass2)

        if return_msg != "":
            return return_msg

        if self.command_dir == "" or len(self.testing_ports) == 0:
            return_msg += ' Please set the autoQC_cfg in the config file!'
            return return_msg

        ## clear chiller temp monitor graph

        self.runClearMonitor_execute(tab, debug)

        ## dic of stages and tests
        keys_stages = list(self.stages_dict.keys())

        ## get panel stage and tests from itkdb
        existing_stage_test_index = [99, 99]
        for idx in range(len(self.autoQC_panels)):
            if self.autoQC_panels[idx] != "":
                panelid = self.autoQC_panels[idx]
                if "/" in self.autoQC_panels[idx]:
                    panelid_splits = self.autoQC_panels[idx].split("/")
                    panelid = panelid_splits[0]
                stage_test = get_panel_stage_tests(client, str(panelid))
                if stage_test[0] in self.stages_dict:
                    idx_stage = keys_stages.index(stage_test[0])
                    idx_test = self.stages_dict["BONDED"].index(stage_test[1])
                    if idx_stage < existing_stage_test_index[0]: # first get the lowest stage in the crate
                        existing_stage_test_index[0] = idx_stage
                        existing_stage_test_index[1] = idx_test
                    if idx_stage == existing_stage_test_index[0] and idx_test < existing_stage_test_index[1]: # then get the least tests in that stage
                        existing_stage_test_index[1] = idx_test
                elif stage_test[0] == "" or stage_test[0] == "SMD_LOAD" or stage_test[0] == "MAN_LOAD":
                    return_msg += "Panel "+panelid+" registration not complete!!"
                else:
                    return_msg += "Panel "+panelid+" beyond Burn-In stage!!"

        if return_msg != "":
            self.autoQC_status = 0
            return return_msg
       
        if existing_stage_test_index[0] == 99:
            existing_stage_test_index[0] = 0

        if existing_stage_test_index[1] == 99:
            existing_stage_test_index[1] = 0

        # update status code
        for idx_stage in range(existing_stage_test_index[0]):
            self.autoQC_status += len(self.stages_dict[keys_stages[idx_stage]])
        self.autoQC_status += (existing_stage_test_index[1] + 1 )
            
        existing_stage = keys_stages[existing_stage_test_index[0]]
        existing_test = self.stages_dict[existing_stage][existing_stage_test_index[1]]
        print("==== Auto QC existing stage: "+existing_stage)
        print("==== Auto QC existing test: "+existing_test)
        
        for idx_stage in range(existing_stage_test_index[0], len(self.stages_dict)):
            current_stage =  keys_stages[idx_stage]
            initial_test_index = 1
            if idx_stage == existing_stage_test_index[0]:
                initial_test_index = existing_stage_test_index[1] + 1
            for idx_test in range(initial_test_index, len(self.stages_dict[current_stage])):
                current_test = self.stages_dict[current_stage][idx_test]
                print("==== Now doing "+current_test+" tests on stage "+current_stage)
                target_temp = float(endTemp)
                if current_test == "cold":
                    target_temp = float(low_temp)
                print("==== first, get stable chiller temp to "+str(target_temp))
                result_target_temp = self.get_stable_temp(target_temp, tab, debug, save_csv, save_influx)
                if result_target_temp != "":
                    self.autoQC_status == 0
                    return result_target_temp

                self.sendCommand("AllTests,"+current_stage+"_"+current_test+"", pass1, pass2)
                if self.autoQC_status == 0:
                    return "AutoQC is stopped!"
                print("==== Upload "+current_test+" tests data for stage "+current_stage)
                self.sendCommand("uploadTestResults", pass1, pass2)
                if self.autoQC_status == 0:
                    return "AutoQC is stopped!"
                self.autoQC_status += 1
            #all tests on this stage done
            #transition to next stage
            if idx_stage < len(self.stages_dict) - 1:
                next_stage = keys_stages[idx_stage+1]
                if next_stage == "THERMAL":
                    print("==== Now start thermal cycling")

                    result_cycle = self.wait_for_thermal_cycle(low_temp, high_temp, downRR, upRR, stay_low, stay_high, nCycle, endTemp, tab, debug, save_csv, save_influx)
                    if result_cycle != "":
                        self.autoQC_status == 0
                        return result_target_temp

                    print("==== panel stage transition to THERMAL")
                    self.sendCommand("panelStageTransition,THERMAL", pass1, pass2)
                    if self.autoQC_status == 0:
                        return "AutoQC is stopped!"
                    self.autoQC_status += 1
                if next_stage == "BURN_IN":
                    print("==== Now start burn-in")

                    target_temp = float(endTemp)
                    print("==== first, get stable chiller temp to "+str(target_temp))
                    result_target_temp = self.get_stable_temp(target_temp, tab, debug, save_csv, save_influx)
                    if result_target_temp != "":
                        self.autoQC_status == 0
                        return result_target_temp

                    self.sendCommand("BurnIn,"+str(burn_in_current_load)+","+str(burn_in_time), pass1, pass2)
                    if self.autoQC_status == 0:
                        return "AutoQC is stopped!"
                    print("==== panel stage transition to BURN_IN")
                    self.sendCommand("panelStageTransition,BURN_IN", pass1, pass2)
                    if self.autoQC_status == 0:
                        return "AutoQC is stopped!"
                    self.autoQC_status += 1

        if not (existing_stage_test_index[0] ==  len(self.stages_dict)-1 and existing_stage_test_index[1] == len(self.stages_dict[existing_stage])-1):
            print("==== Finished all QC steps, turn off everything now")

            target_temp = float(endTemp)
            print("==== first, get stable chiller temp to "+str(target_temp))
            result_target_temp = self.get_stable_temp(target_temp, tab, debug, save_csv, save_influx)
            if result_target_temp != "":
                self.autoQC_status == 0
                return result_target_temp

            print("==== Turn off chiller")
            self.runTurnOff_execute(tab, debug, save_csv, save_influx)
            print("==== Turn off PS")
            self.sendCommand("stop", "fake", "fake")

        self.autoQC_end = time()

        return return_msg

    def runStopAutoQC(self, button, tab, debug):
        if button==None:
            raise PreventUpdate

        self.sendCommand("stop", "fake", "fake")

        self.autoQC_status = 0

        return ''

    def updateAutoQCStatus(self, interval, tab):
    
        ## update autoQC_status in text file
        if self.autoQC_status_copy != self.autoQC_status:
            self.autoQC_status_copy = self.autoQC_status
            if self.command_dir != "":
                with open(self.command_dir+"/autoQC_status_port"+str(self.port)+".txt", 'w') as file_out:
                    file_out.write(str(self.autoQC_status))

        if self.autoQC_status == 0:
            return "Auto QC not running!"

        time_now = time()
        deltaT = time_now - self.autoQC_start
        if self.autoQC_status == 8 or self.autoQC_status == -2:
            deltaT = self.autoQC_end - self.autoQC_start

        else:
            ## make sure chiller is ON
            if not self.chillers_isON[int(tab)]:
                print("=== Chiller got turned OFF unexpectedly! Turning it ON now........")
                self.runTurnOn_execute(tab, 0, [1], [1])
                sleep(3)

        time_diff = timedelta(seconds=deltaT)
        days = time_diff.days
        hours, remainder = divmod(time_diff.seconds, 3600)
        minutes, seconds = divmod(remainder, 60)
        formatted_time = f"{days:02d}:{hours:02d}:{minutes:02d}:{seconds:02d}"

        if self.autoQC_status < 0: ## doing basic tests
            if self.autoQC_status == -2:
                return "Electrical Tests finished! Chiller is still ON. Total time sued: "+formatted_time
            else:
                return "Doing Electrical Tests... Time elapsed: "+formatted_time

        idx_status = 0
        keys_stages = list(self.stages_dict.keys())
        if self.autoQC_status == 8:
            return "Auto QC finished!! Chiller and power supplies are turned OFF. Total time used: "+formatted_time

        status_msg = "Time elapsed: "+formatted_time+": Start"
        for key in keys_stages:
            for test in self.stages_dict[key]:
                if idx_status < self.autoQC_status:
                    if test == "":
                        status_msg += " -> " + key + " (Done)"
                    else:
                        status_msg += " -> " + key + " "+test+" test (Done)"
                elif idx_status ==  self.autoQC_status:
                    if test == "":
                        status_msg += " -> " + key + " (Running)"
                    else:
                        status_msg += " -> " + key + " "+test+" test (Running)"
                else:
                    if test == "":
                        status_msg += " -> " + key + " (To-do)"
                    else:
                        status_msg += " -> " + key + " "+test+" test (To-do)"
                idx_status += 1
       
        status_msg += " -> End"
        return status_msg

    def wait_for_thermal_cycle(self, low_temp, high_temp, downRR, upRR, stay_low, stay_high, nCycle, endTemp, tab, debug, save_csv, save_influx):

        self.chillers_cycle_done[int(tab)] = False

        self.runStartThermalCycle_execute(low_temp, high_temp, downRR, upRR, stay_low, stay_high, nCycle, endTemp, tab, debug, save_csv, save_influx)
        sleep(5)

        if not self.chillers[int(tab)].running:
            return "Can not start thermal cycle!"
        while True:
            if self.autoQC_status == 0:
                return ""
            sleep(5)
            if self.chillers_cycle_done[int(tab)]:
                print("=== Thermal Cycle Finished!!")
                sleep(10)
                return ""

    def get_stable_temp(self, set_temp, tab, debug, save_csv, save_influx):
        # first, establish communication
        if not self.chillers[int(tab)].running:
            self.runStartMonitor_execute(tab, debug, save_csv, save_influx)
            sleep(5)
        if not self.chillers[int(tab)].running:
            return " Can not start the monitoring of chiller"

        # then, check if chiller is ON
        if not self.chillers_isON[int(tab)]:
            print("=== Turn ON chiller now")
            self.runTurnOn_execute(tab, debug, save_csv, save_influx)
            sleep(3)

        # then, set target temp to set_temp
        if (self.chillers_targetTemp[int(tab)] is not None) and (abs(self.chillers_targetTemp[int(tab)] - set_temp) > 0.1):
            self.runSetTemp_execute(str(set_temp), tab, debug, save_csv, save_influx)
            sleep(3)

        # then, wait until measured temp reaches target temp
        dTMax = 2.0
        reachTarget = abs(set_temp - self.chillers_measTemp[int(tab)]) < dTMax
        while (not reachTarget):
            sleep(5)
            reachTarget = abs(set_temp - self.chillers_measTemp[int(tab)]) < dTMax
            if self.autoQC_status == 0:
                return ""
        print("==== target temp "+str(set_temp)+" is reached!")
        return ""
            
    def fillAutoQCPanels(self, pass1, pass2):
        nPanel = len(self.autoQC_panels)
        self.autoQC_versions = [None]*nPanel
        self.autoQC_batchs = [None]*nPanel
        self.autoQC_boards = [None]*nPanel
        self.autoQC_serials = [None]*nPanel

        user = itkdb.core.User(pass1, pass2)
        user.authenticate()
        if not user.is_authenticated():
            print(" Please login to ITK DB first!!!")
            return " Please login to ITK DB first!!!"
        client=itkdb.Client(user=user)


        for idx_panel in range(nPanel):
            panelid_str = self.autoQC_panels[idx_panel]
            if panelid_str != "":
                panelid = panelid_str
                excluded_pos = []
                if "/" in panelid_str:
                    panelid_splits = panelid_str.split("/")
                    panelid = panelid_splits[0]
                    excluded_pos = panelid_splits[1].split(",")
                panel = pwbdb.Component(f'20USBPC{panelid}', client)
                componentType = ""
                try:
                    componentType = panel.componentType
                except:
                    componentType = "InValid"
                if componentType == "PWB_CARRIER":
                    version = None
                    batch = None
                    boards = ['']*10
                    serials = ['']*len(boards)
                    for board,pb in panel.children.get('PWB',{}).items():
                        version = pb.property('VERSION')
                        batch = pb.property('BATCH')
                        serial = pb.property('PB_NUMBWE')
                        if str(board) not in excluded_pos:
                            boards[board] = str(board)
                            serials[board] = str(serial)
                    self.autoQC_versions[idx_panel] = version
                    self.autoQC_batchs[idx_panel] = batch
                    self.autoQC_boards[idx_panel] = boards
                    self.autoQC_serials[idx_panel] = serials

    def sendCommand(self, command, pass1, pass2):

        commands_all = []
        client = None
        if command == "stop":
            for port in self.testing_ports:
                for tab in range(5):
                    command_id = time()
                    command_str = str(command_id)+";"+command
                    commands_all.append(["CMD_port"+str(port)+"_tab"+str(tab)+".txt", command_str])
            
        else:
            user = itkdb.core.User(pass1, pass2)
            user.authenticate()
            if not user.is_authenticated():
                print(" Please login to ITK DB first!!!")
                return " Please login to ITK DB first!!!"
            client=itkdb.Client(user=user)

            for idx_panel in range(len(self.autoQC_panels)):
                panelid_str = self.autoQC_panels[idx_panel]
                port = self.testing_ports[int(idx_panel/5)]
                tab = idx_panel % 5
                if panelid_str != "":
                    panelid = panelid_str
                    if "/" in panelid_str:
                        panelid_splits = panelid_str.split("/")
                        panelid = panelid_splits[0]
                    if self.autoQC_versions[idx_panel] is not None:
                        version = self.autoQC_versions[idx_panel]
                        batch = self.autoQC_batchs[idx_panel]
                        boards = self.autoQC_boards[idx_panel]
                        serials = self.autoQC_serials[idx_panel]

                        boards_str = ",".join(boards)
                        serials_str = ",".join(serials)

                        command_id = time()
                        command_str = str(command_id)+";"+command+";"+panelid+";"+str(version)+";"+str(batch)+";"+boards_str+";"+serials_str+";P1"+string_to_hex(pass1)+";P2"+string_to_hex(pass2)
                        commands_all.append(["CMD_port"+str(port)+"_tab"+str(tab)+".txt", command_str])

        if "panelStageTransition" in command:
            print("=== get command to do panel stage transition, doing it in place here now:")
            new_stage = command.split(",")[1]
            for command_i in commands_all:
                panelName = command_i[1].split(";")[2]
                print("== doing stage transition for panel "+panelName+" to stage: "+new_stage)
                try:
                    panel_transition(client, panelName, new_stage, force=True)
                except Exception:
                    print("=== Get some exception in panel transition...")
        else:
            print("==== sending commands:")
            print(commands_all)
            for command_i in commands_all:
                with open(self.command_dir+"/"+command_i[0], 'w') as command_file:
                    command_file.write(command_i[1])

            if command != "stop":
                print("==== now wait until the commands have all been executed")
                commands_finished = [False]*len(commands_all)
                while True:
                    if self.autoQC_status == 0:
                        return "AutoQC is stopped!"
                    nFinished = sum(commands_finished)
                    if nFinished == len (commands_finished):
                        break

                    print("==== Search for command "+command+" execution result...")
                    sleep(2)

                    for idx_command in range(len(commands_all)):
                        if commands_finished[idx_command]:
                            continue
                        out_path = self.command_dir+"/OUT_"+commands_all[idx_command][0]
                        if os.path.exists(out_path) and os.path.getsize(out_path) > 2:
                            result = ""
                            with open(out_path, 'r') as file_out:
                                result = file_out.readline().strip()
                            if ";" not in result:
                                continue
                            command_id_out = result.split(";")[0]
                            result_out = result.split(";")[1]
                            command_id_in = commands_all[idx_command][1].split(";")[0]
                            if command_id_out == command_id_in and result_out == "Done":
                                commands_finished[idx_command] = True
                                print("Command finished! --> "+commands_all[idx_command][1])
                                os.remove(out_path)
                                continue

        print("=== All commands for "+command+" are finished!!!")
        return ""

