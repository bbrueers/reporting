# v1.9.6
- fix issue with explode for low version of pandas
# v1.9.4
- Add a tab to display test results in the GUI
# v1.9.3
- Update automatic database registration script due to database change
- Fix in test upload script wrt old test result issue
# v1.9.2
- Add a panel stage transition button in GUI
# v1.9.1
- Fix equality comparison when checking consistance with test institute and location
# v1.9.0
- Add a upload test data button
- Add a button to set institution and pass it to `pbv3_mass_test`
# v1.8.1
- Fix --hbipc option support in PowertoolsRunner command building
# v1.8.0
- Show error messages for Auto Fill button in GUI
- Add hybrid burn-in powerboard carrier (hbipc) switch to enable hbipc test sequence in the GUI
# v1.7.0
- Add UI for powersupply initialization and I2C commuication checks
